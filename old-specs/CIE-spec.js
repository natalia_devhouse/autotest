const EC = protractor.ExpectedConditions;
let price;

async function getPage() {
    await browser.get('http://dev-old.cakeiteasy.se/');
    expect(browser.getTitle()).toMatch('Cake it easy');
}

function goToBakery(type = '') {
    $('[role="search"] input').click().sendKeys('autotest '+ type).sendKeys(protractor.Key.ENTER);

    browser.wait(() => {
        return browser.getCurrentUrl().then((url) => {
            expect(url).toMatch('Autotest%20' + type);
            return url;
        });
    });
}

function chooseCake(count) {
    //count of cakes begins from 1
    $('.search-items > div > div:nth-child('+count+')').click();
    browser.wait(() => {
        return browser.getCurrentUrl().then((url) => {
            expect(url).toMatch('build-cake');
            return url;
        });
    });
}

function customizeCake(count) {
    const btnGoToDelivery = $('[ng-click*=goToDelivery]');
    $('.cake-size:nth-child('+ count +')').click();
    expect(btnGoToDelivery.getAttribute('disabled')).toMatch('disabled|true');
    $('.cake-fillings .iradio_square-green').click();
    $('.cake-motive').isPresent().then((result) => {
        if (result) {
            // expect(btnGoToDelivery.getAttribute('disabled')).toMatch('disabled|true');
            $('.cake-motive .iradio_square-green').click();
        }
    });

    btnGoToDelivery.click();
    browser.wait(() => {
        return browser.getCurrentUrl().then((url) => {
            expect(url).toMatch('delivery');
            return url;
        });
    });
}

function deliveryPickUp() {
    $('.choose-method > .choose-case:first-child button').click();
    const goToPayment = $('[ng-click*=goToPayment]');
    expect(goToPayment.getAttribute('disabled')).toMatch('disabled|true');
    $('[ng-class*=date]').click();
    $('[ng-class*=date] + ul td[aria-disabled=false]').click();
    $('.panel-body select').click();
    $('.panel-body select option:nth-child(2)').click();
    expect(goToPayment.getAttribute('disabled')).toBeNull();
    goToPayment.click();
    browser.wait(() => {
        return browser.getCurrentUrl().then((url) => {
            expect(url).toMatch('payment');
            return url;
        });
    });
}

function deliveryToHome() {
    $('.choose-method > .choose-case:last-child button').click();
    const goToPayment = $('[ng-click*=goToPayment]');
    $('.post-number').click().sendKeys('11234');
    $('[ng-model*=date]').click();
    $('[ng-model*=date] + ul td[aria-disabled=false]').click();
    $('select[ng-model*=timeRange]').click();
    $('select[ng-model*=timeRange] option[value="1"]').click();
    $('[ng-model*=address]').sendKeys('Test Street');
    $('[ng-model*=userName]').sendKeys('Natalia');
    $('[ng-model*=phone]').sendKeys('777777');
    goToPayment.click();
    browser.wait(() => {
        return browser.getCurrentUrl().then((url) => {
            expect(url).toMatch('payment');
            return url;
        });
    });
}

function payInStore() {
    $('.choose-method .choose-case:first-child button').click();
    $('[ng-model*=userName]').sendKeys('Natalia');
    $('[ng-model*=phone]').sendKeys('777777');
    $('[ng-model*=email]').sendKeys('autotest.devhouse@gmail.com');
    $('[ng-click*=addPayInStoreOrder]').click();
    browser.wait(() => {
        return browser.getCurrentUrl().then((url) => {
            expect(url).toMatch('completed=true');
            return url;
        });
    });
}

function confirmOrder() {
    browser.executeScript('window.localStorage.setItem("cakeiteasy-a/b-private-payment-sweden", JSON.stringify({"version": 1}))');
	browser.driver.navigate().refresh();
    $('.choose-method .choose-case:first-child button').click();
    $('[ng-model*=userName]').sendKeys('Natalia');
    $('[ng-model*=phone]').sendKeys('777777');
    $('[ng-model*=email]').sendKeys('autotest.devhouse@gmail.com');
    $('.icheckbox_square-green').click();
    $('[ng-click*=addBamboraOrder]').click();

    browser.wait(() => {
        return $('.bc-overlay-container').isDisplayed();
    });
}

function bamboraCheckout() {
    browser.switchTo().frame($('.bc-overlay-container .bc-iframe-container iframe').getWebElement()).then(function () {
        $('#panel_paymentcard > div > div > bec-payment-card-start > form > bec-credit-card-form > div > div:nth-child(1) > h2 > a').click();
        $('#democard-controller > bec-demo-cards > button:nth-child(1)').click();
        $('#payBtn > div:nth-child(1)').click();
		browser.wait(EC.urlContains('receipt'));
    });
}

function invoicePayment() {
    $('.choose-method .choose-case:last-child button').click();
    $('.payment-info-step .choose-method .choose-case:last-child button').click();
    $('body > div.ng-scope > div.container-fluid.ng-scope > div > div > div.page-content.ng-scope > div > div.container.stage-container > div:nth-child(2) > div > div:nth-child(3) > div.few-steps-page.well.ng-scope > div.ng-scope > div:nth-child(2) > div > div > div.row > div > div > button').click();
    browser.wait(() => {
        return browser.getCurrentUrl().then((url) => {
            expect(url).toMatch('receipt');
            return url;
        });
    });
}

async function login() {
    $('.login-menu-section .login-button').click();
    $('.modal-dialog [ng-model*=email]').click().sendKeys('autotest.devhouse@gmail.com');
    $('.modal-dialog [ng-model*=password]').click().sendKeys('ciedevhouse');
    $('.modal-dialog button[type=submit]').click();
    await browser.wait(() => {
        return browser.getCurrentUrl().then((url) => {
			expect(EC.visibilityOf($('.login-menu-section .user-name'))).toBeTruthy();
            expect($('.login-menu-section .user-name').getText()).toMatch('Autotest');
            return url;
        });
    });
}

fdescribe('E2E: Pickup-PayInStore', () => {
	it('Pickup-PayInStore: open store', () => {
		getPage();
	});

	it('Pickup-PayInStore: go to bakery', () => {
		goToBakery('PayInStore');
	});

	it('Pickup-PayInStore: choose cake', () => {
		chooseCake(2);
	});

	it('Pickup-PayInStore: customize cake', () => {
		customizeCake(2);
	});

	it('Pickup-PayInStore: customize pick-up delivery', () => {
		deliveryPickUp();
	});

	it('Pickup-PayInStore: paying in store', () => {
		price = $('.basket-price .pull-right').getText();
		expect(price).toMatch('250');
		payInStore();
	});
});

describe('E2E: HomeDelivery-PayInStore', () => {
	it('HomeDelivery-PayInStore: open store', () => {
		getPage();
	});

	it('HomeDelivery-PayInStore: go to bakery', () => {
		goToBakery('PayInStore');
	});

	it('HomeDelivery-PayInStore: choose cake', () => {
		chooseCake(2);
	});

	it('HomeDelivery-PayInStore: customize cake', () => {
		customizeCake(1);
	});

	it('HomeDelivery-PayInStore: customize home delivery', () => {
		deliveryToHome();
	});

	it('HomeDelivery-PayInStore: paying in store', () => {
		price = $('.basket-price .pull-right').getText();
		expect(price).toMatch('450');
		payInStore();
	});
});

describe('E2E: Pickup-Bambora', () => {
	it('Pickup-Bambora: open store', () => {
		getPage();
	});

	it('Pickup-Bambora: go to bakery', () => {
		goToBakery();
	});

	it('Pickup-Bambora: choose cake', () => {
		chooseCake(2);
	});

	it('Pickup-Bambora: customize cake', () => {
		customizeCake(2);
	});

	it('Pickup-Bambora: customize pick-up delivery', () => {
		deliveryPickUp();
	});

	it('Pickup-Bambora: confirm order', () => {
		price = $('.basket-price .pull-right').getText();
		expect(price).toMatch('250');
		confirmOrder();
	});

	it('Pickup-Bambora: bambora checkout', () => {
		bamboraCheckout();
	});
});

describe('E2E: Pickup-Invoice', () => {
	it('Pickup-Invoice: open store', () => {
		getPage();
	});

	it('Pickup-Invoice: go to bakery', () => {
		goToBakery();
	});

	it('Pickup-Invoice: choose cake', () => {
		chooseCake(2);
	});

	it('Pickup-Invoice: customize cake', () => {
		customizeCake(2);
	});

	it('Pickup-Invoice: customize pick-up delivery', () => {
		deliveryPickUp();
	});

	it('Pickup-Invoice: login', () => {
		login();
	});

	it('Pickup-Invoice: invoice payment', () => {
		invoicePayment()
	});
});