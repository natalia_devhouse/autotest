const EC = protractor.ExpectedConditions;
let price,
	tests=[{"enabled":true,"owner":"Natalia","testgroup":"CIE-spec.js","testcase":"E2E: Pickup-PayInStore"},{"enabled":true,"owner":"Natalia","testgroup":"CIE-spec.js","testcase":"E2E: HomeDelivery-PayInStore"},{"enabled":true,"owner":"Natalia","testgroup":"CIE-spec.js","testcase":"E2E: Pickup-Bambora"},{"enabled":true,"owner":"Natalia","testgroup":"CIE-spec.js","testcase":"E2E: Pickup-Invoice"},{"enabled":true,"owner":"Natalia","testgroup":"NS3-spec"}]


async function getPage() {
	await browser.get('http://admin-dev.v3.cakeiteasy.no/');
	expect(EC.urlContains('login')).toBeTruthy();
}

async function createOrder() {
	// await browser.wait(EC.presenceOf($('.bakeries-list > div:nth-child(2)')));
	await browser.sleep(3000);
	await $('.bakeries-list > div:nth-child(5)').click();

	await browser.wait(EC.presenceOf($('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div > order-create-settings-products > div > div.mt-1 > div > div > div > button.btn.btn-outline-primary.btn-sm.ng-star-inserted')));
	await $('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div > order-create-settings-products > div > div.mt-1 > div > div > div > button.btn.btn-outline-primary.btn-sm.ng-star-inserted').click();
	// select cake
	await $('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div > order-create-settings-products > div > div.ng-star-inserted > div > div > order-new-product-line > div > div.row > div > div > select').click();
	await $('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div > order-create-settings-products > div > div.ng-star-inserted > div > div > order-new-product-line > div > div.row > div > div > select option:nth-child(1)').click();

	// select size
	await browser.sleep(1000);
	// await browser.wait(EC.presenceOf($('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div > order-create-settings-products > div > div.ng-star-inserted > div > div > order-new-product-line > div > div.row > div > div:nth-child(2) > select')));
	await $('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div > order-create-settings-products > div > div.ng-star-inserted > div > div > order-new-product-line > div > div.row > div > div:nth-child(2) > select').click();
	await $('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div > order-create-settings-products > div > div.ng-star-inserted > div > div > order-new-product-line > div > div.row > div > div:nth-child(2) > select option:nth-child(2)').click();

	// select filling
	await browser.sleep(1000);
	// await browser.wait(EC.presenceOf($('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div > order-create-settings-products > div > div.ng-star-inserted > div > div > order-new-product-line > div > div.row > div:nth-child(1) > div:nth-child(3) > select')));
	await $('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div > order-create-settings-products > div > div.ng-star-inserted > div > div > order-new-product-line > div > div.row > div:nth-child(1) > div:nth-child(3) > select').click();
	await $('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div > order-create-settings-products > div > div.ng-star-inserted > div > div > order-new-product-line > div > div.row > div:nth-child(1) > div:nth-child(3) > select option:nth-child(2)').click();

	// continue
	await browser.wait(EC.presenceOf($('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div > order-create-settings-products > div > div.ng-star-inserted > div > div > order-new-product-line > div > div.text-right > button.btn.btn-primary')));
	$('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div > order-create-settings-products > div > div.ng-star-inserted > div > div > order-new-product-line > div > div.text-right > button.btn.btn-primary').click();


	// pickup
	await browser.wait(EC.presenceOf($('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div.col-lg-5.ng-star-inserted > order-create-settings-delivery > div > choose-delivery-order > div > div.ng-star-inserted > div > search-bar > div > input')));
	$('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div.col-lg-5.ng-star-inserted > order-create-settings-delivery > div > choose-delivery-order > div > div.ng-star-inserted > div > search-bar > div > input').click();
	$('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div.col-lg-5.ng-star-inserted > order-create-settings-delivery > div > choose-delivery-order > div > div.ng-star-inserted > div > search-bar > div > input').sendKeys(protractor.Key.ENTER);

	// date
	$('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div.col-lg-5.ng-star-inserted > order-create-settings-delivery > div > choose-delivery-order > div > div.ng-star-inserted > div:nth-child(2) > div > div > div > input').click();
	$('ngb-datepicker .ngb-dp-arrow.right').click();
	$('ngb-datepicker .ngb-dp-day:not(.disabled)').click();

	//time
	$('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div.col-lg-5.ng-star-inserted > order-create-settings-delivery > div > choose-delivery-order > div > div.ng-star-inserted > div:nth-child(2) > div > div.col-sm-6.ng-star-inserted > select').click();
	$('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div.col-lg-5.ng-star-inserted > order-create-settings-delivery > div > choose-delivery-order > div > div.ng-star-inserted > div:nth-child(2) > div > div.col-sm-6.ng-star-inserted > select option:nth-child(2)').click();

	// recepient and phone
	$('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div.col-lg-5.ng-star-inserted > order-create-settings-delivery > div > choose-delivery-order > div > div:nth-child(3) > div > div > div:nth-child(1) > input').sendKeys('Natalia');
	$('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div > div.col-lg-5.ng-star-inserted > order-create-settings-delivery > div > choose-delivery-order > div > div:nth-child(3) > div > div > div:nth-child(2) > div > input').sendKeys('8463');

	// continue
	$('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div > create-bakery-order > div:nth-child(2) > div > div.mt-2.text-right.mb-1.ng-star-inserted > div > button').click();

	await browser.sleep(3000);
	// await browser.wait(EC.presenceOf($('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div.mb-1.ng-star-inserted > button')));
}

describe('MO with many orders', () => {
	it('get page and login', async () => {
		getPage();
		await browser.sleep(2000);
		await $('input[ng-reflect-name="email"]').sendKeys('natalia.d@devhouse.pro');
		await $('input[type="password"]').sendKeys('ciedevhouse');
		await browser.sleep(1000);
		await $('button[type=submit]').click();
		await browser.wait(EC.urlContains('order-manager'));
	});

	xit('find customer', async () => {
		await browser.sleep(2000);
		await $('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-list > div > div > div > search-orders-bar > div.d-flex.flex-row.w-100 > div:nth-child(2) > button').click();
		expect(EC.urlContains('drafts/details')).toBeTruthy();
		await $('find-customer input').sendKeys('natalia.d@dev').sendKeys(protractor.Key.ARROW_DOWN).sendKeys(protractor.Key.ENTER);
		await browser.sleep(2000);
	});

	it('continue draft', async () => {
		await browser.get('http://admin-dev.v3.cakeiteasy.no/entire/SE/areas/3/order-manager/drafts/details?orderId=17537');
		// await browser.sleep(2000);
		// await $('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > nav > ul > li:nth-child(3) > a').click();
		// expect(EC.urlContains('order-manager/drafts')).toBeTruthy();
		// await browser.sleep(4000);
		// await browser.actions().mouseMove($('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > div > div > div > div > div > div:nth-child(3) > div > div')).perform();
		// await $('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > div > div > div > div > div > div:nth-child(3) > div > div > div.col-sm-5.text-right > div.btn.btn-outline-primary.go-to-details-btn').click();
		await browser.sleep(3000);
		await $('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div.mb-1.ng-star-inserted > button').click();
	});

	it('create MO', async () => {
		for (let i=1; i<=18; i++) {
			await createOrder();
			await $('#app > div > div.app-root-item.app-root-content > div > ng-component > ng-component > order-manager > div > orders-drafts > multi-order-create > div > div > div > div:nth-child(3) > div.mb-1.ng-star-inserted > button').click();
		}

		await createOrder();
		await browser.refresh();
	});

});

