const FrontPage = require('../page-objects/front-page');
const OverviewPage = require('../page-objects/overview-page');
const DeliveryPage = require('../page-objects/delivery-page');
const CakeBuilder = require('../page-objects/cake-builder');
const PaymentPage = require('../page-objects/payment-page');
const ProfilePage = require('../page-objects/profile-page');
const CartActions = require('../page-objects/cart-actions');

const EC = protractor.ExpectedConditions,
	cakePrice = 300,
	withMotivePrice = cakePrice + 30,
	withTextPrice = withMotivePrice + 20,
	withExtraPrice = withTextPrice + 150,
	fullPrice = withExtraPrice + 160,
	platform = browser.isMobile ? ' mobile' : ' desktop';

if (browser.browserName === "Safari") {
	browser.driver.manage().window().maximize();
}

function waitUrlChange () {
	return function () {
		return browser.getCurrentUrl().then(function(url) {
			return url;
		});
	}
}

describe('NS3: actions from front page' + platform, () => {

	const frontPage = new FrontPage();
	const overviewPage = new OverviewPage();

	it('Front page: Get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
	});

	it('Front page: login', async () => {
		$('language-selector + button + button').click();
		await frontPage.login();
		expect(await $('user-dropdown-menu .dropdown button').getText()).toMatch('Autotest');
		await frontPage.getPage();
	});

	xit('Front page: Open bakery from bakery section', async () => {
		await browser.wait(EC.presenceOf(frontPage.bakeryItem));
		await browser.wait(EC.elementToBeClickable(frontPage.bakeryItem));
		frontPage.bakeryItem.click();
		await browser.wait(EC.urlContains('order-cake'));
		await browser.wait(EC.presenceOf(overviewPage.bakeryOverviewTitle));
		await browser.wait(EC.textToBePresentInElement(overviewPage.bakeryOverviewTitle, overviewPage.bakeryOverviewTitleText));
		expect(await overviewPage.bakeryOverviewTitle.getText()).toMatch(overviewPage.bakeryOverviewTitleText);
		$('header-layout .header__logo').click();
		await browser.wait(waitUrlChange());
	});

	it('Front page: Open city from cities section', async () => {
		await browser.wait(EC.presenceOf(frontPage.cityItem));
		await browser.wait(EC.elementToBeClickable(frontPage.cityItem));
		frontPage.cityItem.click();
		await browser.wait(EC.urlContains('order-cake/oslo'));
		await browser.wait(EC.presenceOf(overviewPage.cityOverviewTitle));
		expect(await overviewPage.cityOverviewTitle.getText()).toMatch(overviewPage.cityOverviewTitleText);
		await browser.wait(EC.invisibilityOf($('.cakey-loader')));
		await browser.wait(EC.presenceOf(overviewPage.getCake(0).$('.cake-card-footer')));
		$('header-layout .header__logo').click();
		await browser.wait(waitUrlChange());
	});

	it('Front page: Open how it works modal', async () => {
		await browser.wait(EC.presenceOf(frontPage.footer));
		frontPage.howItWorksLink.click();
		await browser.wait(EC.presenceOf($('.how-it-works-modal')));
		expect($('.how-it-works-modal').isDisplayed()).toBeTruthy();
		$('.modal .icon-close').click();
	});

	it('Front page: Open faq', async () => {
		frontPage.helpLink.click();
		await browser.wait(EC.presenceOf($('.modal')));
		await $('.modal .modal-header').getText().then((result) => {
			expect(result.toLowerCase()).toMatch('contact us');
		});
		$('.modal .icon-close').click();
	});
});

describe('NS3: City Overview' + platform, () => {

	const frontPage = new FrontPage();
	const overviewPage = new OverviewPage();

	it('City Overview: Get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
	});

	it('City Overview: Find city', async () => {
		await frontPage.findCity();
		expect(await browser.getTitle()).toMatch('Order cake online | Cakes in Jevnaker | Cake it easy');
	});

	it('City Overview: open pickup map', async () => {
		await browser.wait(EC.invisibilityOf($('.cakey-loader')));
		await browser.wait(EC.presenceOf(overviewPage.getCake(0)));
		await browser.wait(EC.elementToBeClickable(overviewPage.pickupLink));
		overviewPage.pickupLink.click();
		await browser.wait(EC.presenceOf($('.modal.show')));
		await browser.wait(EC.presenceOf($('.snazzy-title')));
		await browser.wait(EC.textToBePresentInElement($('.snazzy-title'), 'Autotest bakery!'));
		expect(await  $('.snazzy-title > div').getText()).toMatch('Autotest bakery!');
		$$('.modal-body .icon-close').first().click();
	});

	it('City Overview: open delivery tooltip', async () => {
		await overviewPage.deliveryLink.click();
		await browser.wait(EC.visibilityOf(overviewPage.deliveryModal));
		expect(await  overviewPage.checkDeliveryCitiesList.getText())
			.toMatch('Gran, Hole, Jevnaker, Ringerike, Sør-Aurdal');
		$('.modal .icon-close').click();
	});

	(browser.isMobile ? xit : it)('City Overview: check filters panel working', async () => {
		const imageFilter = $('.search-cakes-panel ui-checkbox-btn button'),
			allergensFilter = $('.search-cakes-panel ui-multi-select');
		await browser.wait(EC.elementToBeClickable(imageFilter));
		imageFilter.click();
		await browser.wait(EC.presenceOf($('.btn-filter-panel')));
		await browser.wait(EC.invisibilityOf($('.cakey-loader')));
		await browser.wait(EC.presenceOf(overviewPage.getCake(0)));
		expect(await overviewPage.getCake(0).$('.cake-name').getText()).toMatch('Macaron Cake');
		allergensFilter.$('button').click();
		await browser.wait(EC.visibilityOf($('.search-cakes-panel ui-multi-select .dropdown-menu')));
		allergensFilter.$$('.dropdown-menu ui-checkbox').first().click();
		allergensFilter.$('.dropdown-menu > div > div').click();
		await browser.wait(EC.visibilityOf($('.no-cakes-found-text')));
		expect($('.no-cakes-found-text').isDisplayed()).toBeTruthy();
		$('.filter-tags-container .btn-filter-panel.btn-pink').click();
	});

	(browser.isMobile ? it : xit)('City Overview: check filters panel working', async () => {
		const filtersButton = $('.search-filter');
		const imageFilter = $('.mobile-search-filters .filters > div:nth-child(2) ui-checkbox'),
			allergensFilter = $('.mobile-search-filters .filters > div:nth-child(1) ui-checkbox'),
			applyFilter = $('.mobile-search-filters .footer button');

		await browser.wait(EC.elementToBeClickable(filtersButton));
		filtersButton.click();
		await browser.wait(EC.presenceOf($('.mobile-search-filters')));

		await browser.wait(EC.elementToBeClickable(imageFilter));
		await imageFilter.click();
		await applyFilter.click();
		await browser.wait(EC.presenceOf($('.btn-filter-panel')));
		await browser.wait(EC.invisibilityOf($('.cakey-loader')));
		await browser.wait(EC.presenceOf(overviewPage.getCake(0)));
		expect(await overviewPage.getCake(0).$('.cake-name').getText()).toMatch('Macaron Cake');

		await filtersButton.click();
		await allergensFilter.click();
		await applyFilter.click();
		await browser.wait(EC.visibilityOf($('.no-cakes-found-text')));
		expect($('.no-cakes-found-text').isDisplayed()).toBeTruthy();
		$('.filter-tags-container .btn-filter-panel.btn-pink').click();
	});

	it('City Overview: Check delivery', async () => {
		await browser.waitForAngularEnabled(true);
		const postNumberInput = $('post-number-delivery-check input'),
			searchButton = browser.isMobile ? $('post-number-delivery-check button.btn-lg') : $('post-number-delivery-check button.btn-slim'),
			checkResult = $('.found-city');
		if (browser.isMobile) {
			await $('.mobile-search-filters button:not(.search-filter)').click();
		} else {
			await $('delivery-check-panel #checkDeliveryDropdown').click();
		}
		postNumberInput.sendKeys('3520');
		expect(EC.presenceOf(searchButton));
		expect(await  checkResult.getText()).toMatch('Jevnaker');
		postNumberInput.clear();
		postNumberInput.sendKeys('0001');
		expect(EC.presenceOf(searchButton));
		expect(await checkResult.getText()).toMatch('Oslo');
		searchButton.click();
		await browser.wait(EC.urlContains('oslo'));
		await browser.wait(EC.presenceOf($('.btn-filter-panel')));
		expect(await  $('.filter-tags-container .btn-filter-panel.btn-gray').getText()).toMatch('Delivery to');
	});
});

describe('NS3: Bakery Profile' + platform, () => {

	const frontPage = new FrontPage();

	it('Bakery Profile: Get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
	});

	it('Bakery Profile: find bakery', async () => {
		await frontPage.findBakery();
		expect(await browser.getTitle()).toMatch('Order cake from Autotest bakery! | Cake it easy');
	});

	it('Bakery Profile: Check delivery availability', async () => {
		const postCodeInput = $$('bakery-delivery-check input').first();
		if (browser.isMobile) {
			$('bakery-profile-collapser button') .click();
		}
		await browser.wait(EC.elementToBeClickable($('bakery-profile .delivery-info-switcher button:last-child')));
		await $('bakery-profile .delivery-info-switcher button:last-child').click();
		postCodeInput.sendKeys('6515');
		await browser.wait(EC.presenceOf($('bakery-delivery-check > div:nth-of-type(3)')));
		expect(await $('bakery-delivery-check > div:last-child').getText()).toMatch('No delivery');
		postCodeInput.clear();
		postCodeInput.sendKeys('3525');
		expect(EC.presenceOf($('bakery-delivery-check .delivery-days'))).toBeTruthy();
	});
});

describe('NS3: Order Pickup and Private Payment' + platform, () => {

	const frontPage = new FrontPage();
	const overviewPage = new OverviewPage();
	const deliveryPage = new DeliveryPage();
	const paymentPage = new PaymentPage();
	const cakeBuilder = new CakeBuilder();

	it('Bakery Profile: Get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
	});

	it('Bakery Profile: find bakery', async () => {
		await frontPage.findBakery();
		expect(await browser.getTitle()).toMatch('Order cake from Autotest bakery! | Cake it easy');
	});

	it('Order Pickup and Private Payment: add pieces cake with one variant to cart', async () => {
		if (browser.isMobile) {
			await overviewPage.openCakeBuilderMobile(0);
		} else {
			await overviewPage.addPiecesCake();
			expect($$('cart-btn > button').first().isDisplayed()).toBeTruthy();
		}
	});

	it('Order Pickup and Private Payment: Go to delivery', async() => {
		if (browser.isMobile) {
			await cakeBuilder.goToDelivery();
		} else {
			await overviewPage.goToDeliveryFromCart();
		}
		expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
	});

	it('Order Pickup and Private Payment: Pickup delivery', async () => {
		await deliveryPage.pickupDelivery();
		expect(await browser.getTitle()).toMatch('Choose payment | Cake it easy');
	});

	it('Order Pickup and Private Payment: Payment', async () => {
		await paymentPage.choosePaymentType(true, false);
	});

	it('Order Pickup and Private Payment: Klarna Payment', async () => {
		await paymentPage.klarnaPayment();
		expect(await browser.getTitle()).toMatch('Receipt | Cake it easy');
	});
});

describe('NS3: Order Home Delivery and Business Card Payment' + platform, () => {

	const frontPage = new FrontPage();
	const overviewPage = new OverviewPage();
	const deliveryPage = new DeliveryPage();
	const cakeBuilder = new CakeBuilder();
	const paymentPage = new PaymentPage();

	it('Order Home Delivery and Business Card Payment: get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
		await frontPage.clearCart();
	});

	it('Order Home Delivery and Business Card Payment: find bakery', async () => {
		await frontPage.findBakery('cities-overview .search-location input');
		expect(await browser.getTitle()).toMatch('Order cake from Autotest bakery! | Cake it easy');
	});

	it('Order Home Delivery and Business Card Payment: open cake builder', async () => {
		if (browser.isMobile) {
			await overviewPage.openCakeBuilderMobile(1);
		} else {
			await overviewPage.openCakeBuilder(1);
		}
		expect($('.cake-builder-modal').isDisplayed()).toBeTruthy();
	});

	it('Order Home Delivery and Business Card Payment: customise cake', async () => {
		await cakeBuilder.chooseCake();
		expect(await cakeBuilder.totalSum.getText()).toMatch(cakePrice.toString());
		await cakeBuilder.chooseImage();
		expect(await  cakeBuilder.totalSum.getText()).toMatch(withMotivePrice.toString());
		await cakeBuilder.addText();
		expect(await  cakeBuilder.totalSum.getText()).toMatch(withTextPrice.toString());
		await cakeBuilder.chooseExtraWithManyVariants();
		expect(await  cakeBuilder.totalSum.getText()).toMatch(withExtraPrice.toString());
		await cakeBuilder.chooseExtraWithOneVariant();
		expect(await  cakeBuilder.totalSum.getText()).toMatch(fullPrice.toString());
	});

	it('Order Home Delivery and Business Card Payment: go to delivery', async () => {
		await cakeBuilder.goToDelivery();
		if (!browser.isMobile) {
			expect(await  $$('.side-cart-wrapper .cart-component-footer div:last-child').first().getText()).toMatch(fullPrice.toString());
		}
		expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
	});

	it('Order Home Delivery and Business Card Payment: Home delivery', async () => {
		await deliveryPage.homeDelivery();
		expect(await browser.getTitle()).toMatch('Choose payment | Cake it easy');
	});

	it('Order Home Delivery and Business Card Payment: Payment', async () => {
		await paymentPage.choosePaymentType(false, true);
		expect($('bambora-payment').isDisplayed()).toBeTruthy();
		await paymentPage.cardPayment(false);
	});

	it('Bambora Checkout', () => {
		paymentPage.bamboraCheckout();
		expect(browser.getTitle()).toMatch('Receipt | Cake it easy');
	});

});

describe('NS3: Order Pickup and Business Invoice Payment' + platform, () => {

	const frontPage = new FrontPage();
	const overviewPage = new OverviewPage();
	const deliveryPage = new DeliveryPage();
	const paymentPage = new PaymentPage();
	const cakeBuilder = new CakeBuilder();

	it('Order Pickup and Business Invoice Payment: get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
		await frontPage.clearCart();
	});

	it('Order Pickup and Business Invoice Payment: find bakery', async () => {
		await frontPage.findBakeryFromHelpModal();
		expect(await browser.getTitle()).toMatch('Order cake from Autotest bakery! | Cake it easy');
	});

	it('Order Pickup and Business Invoice Payment: add pieces cake with one variant to cart', async () => {
		if (browser.isMobile) {
			await overviewPage.openCakeBuilderMobile(0);
		} else {
			await overviewPage.addPiecesCake();
			expect($$('cart-btn > button').first().isDisplayed()).toBeTruthy();
		}
	});

	it('Order Pickup and Business Invoice Payment: Go to delivery', async() => {
		if (browser.isMobile) {
			cakeBuilder.goToDelivery();
		} else {
			await overviewPage.goToDeliveryFromCart();
		}
		expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
	});

	it('Order Pickup and Business Invoice Payment: Pickup delivery', async () => {
		await deliveryPage.pickupDelivery();
		expect(await browser.getTitle()).toMatch('Choose payment | Cake it easy');
	});

	it('Order Pickup and Business Invoice Payment: Payment', async () => {
		await paymentPage.choosePaymentType(false, false);
		if (!browser.userIsLogged) {
			expect($('.modal register-form').isDisplayed()).toBeTruthy();
			$('.have-account-btn').click();
			await frontPage.login();
			if (!browser.isMobile) {
				expect(await $$('user-dropdown-menu .dropdown button').first().getText()).toMatch('Autotest');
			}
		}
	});

	it('Order Pickup and Business Invoice Payment: Invoice payment', async () => {
		await paymentPage.invoicePayment();

		expect(await browser.getTitle()).toMatch('Receipt | Cake it easy');
	});
});

(browser.browserName == "Safari" || browser.isMobile ? xdescribe : describe)('Account pages' + platform, function () {

	const frontPage = new FrontPage();
	const profilePage = new ProfilePage();

	it('Account pages: get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
	});

	it('Account pages: login', async () => {
		if (!browser.userIsLogged) {
			$('language-selector + button + button').click();
			await frontPage.login();
			expect(await $('user-dropdown-menu .dropdown button').getText()).toMatch('Autotest');
		} else {
			await frontPage.goToProfile();
		}
	});

	it('Account pages: invoice info', async () => {
		await profilePage.goToInvoiceInfo();
		expect(browser.getTitle()).toMatch('Payment Preferences | Cake it easy');

		await profilePage.changeInvoiceInfo();
		expect(await profilePage.companyName.getAttribute("value")).toMatch('MERCK AB');
		expect(await profilePage.contactPerson.getAttribute("value")).toMatch('Autotest Devhouse');

		await profilePage.removeInvoiceInfo();
		expect(await profilePage.companyName.getAttribute("value")).toMatch('Autotest Company');
		expect(await profilePage.userEmail.getAttribute("value")).toMatch('autotest.devhouse@gmail.com');
	});

	xit('Account pages: add delivery address', async () => {
		await profilePage.goToDeliveryAddresses();

	});

	it('Account pages: add new calendar on reminders page', async () => {
		await profilePage.goToRemindersPage();
		await profilePage.addCalendar();
		expect(EC.presenceOf($('.reminder:not(.new-reminder)')));
	});

	it('Account pages: remove person from calendar', async () => {
		await $$('.reminder:not(.new-reminder) .action-col i').first().click();
	});

	it('Account pages: upload reminders', async () => {
		await $('.panel-box .pb-header button').click();
		await browser.wait(EC.presenceOf($('.modal')));
		await profilePage.uploadReminder(false);
		await $('.modal button').click();
		await profilePage.uploadReminder(true);
		expect(EC.presenceOf($('reminder:not(.new-reminder)')));
	});

	it('Account pages: edit calendar name', async () => {
		await profilePage.editCalendarName();
		expect(await $('.panel-box ui-select button span').getText()).toMatch('Work birthdays');
	});

	it('Account pages: remove calendar', async () => {
		await profilePage.removeCalendar();
		expect(EC.stalenessOf($('.new-reminder'))).toBeTruthy();
	});
});

(browser.isMobile ? xdescribe : describe)('Cart actions', () => {
	const frontPage = new FrontPage();
	const overviewPage = new OverviewPage();
	const deliveryPage = new DeliveryPage();
	const cakeBuilder = new CakeBuilder();
	const cartActions = new CartActions();

	it('Cart actions: get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
	});

	it('Cart actions: find bakery', async () => {
		await frontPage.findBakery();
		expect(await browser.getTitle()).toMatch('Order cake from Autotest bakery! | Cake it easy');
	});

	it('Cart actions: add cakes to cart', async () =>  {
		await overviewPage.addPiecesCake();
		expect($$('cart-btn > button').first().isDisplayed()).toBeTruthy();
		await overviewPage.openCakeBuilder(1);
		expect($('.cake-builder-modal').isDisplayed()).toBeTruthy();
		await cakeBuilder.chooseCake();
		await cakeBuilder.addMoreCakes();
	});

	it('Cart actions: edit cake from overview page', async () => {
		await cartActions.editCake();
		await cakeBuilder.chooseImage();
		await cakeBuilder.saveCake();
	});

	it('Cart actions: change quantity of cakes', async () => {
		await cartActions.changeQuantityOfCake();
	});

	it('Cart actions: go to delivery', async () => {
		await overviewPage.goToDeliveryFromCart();
		expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
	});

	it('Cart actions: pickup delivery', async () => {
		await deliveryPage.pickupDelivery();
		expect(await browser.getTitle()).toMatch('Choose payment | Cake it easy');
	});

	it('Cart actions: edit delivery', async () => {
		await cartActions.editDelivery();
		expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
	});

	it('Cart actions: home delivery', async () => {
		await deliveryPage.homeDelivery();
		expect(await browser.getTitle()).toMatch('Choose payment | Cake it easy');
	});

	it('Cart actions: delete cake from cart', async () => {
		await cartActions.deleteCake();
	});

	it('Cat actions: clear cart', async () => {
		await cartActions.clearCart();
	});

	it('Cart actions: try to add cake from other bakery', async () => {
		await overviewPage.addPiecesCake();
		await frontPage.findBakery('.search-location input', 'Che Bakery');
		await overviewPage.openCakeBuilder(0);
		await cakeBuilder.chooseCake();
		await $$('.build-section-footer .btn-footer').get(1).click();
		expect(EC.presenceOf($('.modal:not(.cake-builder-modal)')));
	});

	it('Cart actions: work of modal for change cart to other bakery', async () => {
		await $('.modal:not(.cake-builder-modal) .modal-footer button:last-child').click();
		expect(EC.stalenessOf($('.modal:not(.cake-builder-modal)')));
		await browser.wait(EC.presenceOf($('default-busy')));
		await browser.wait(EC.stalenessOf($('default-busy')));
		await $$('.build-section-footer .btn-footer').get(1).click();
		expect(EC.presenceOf($('.modal:not(.cake-builder-modal)')));
		await $('.modal:not(.cake-builder-modal) .modal-footer button:first-child').click();
		expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
		expect(EC.textToBePresentInElement($('.cl-cake-info > div:nth-child(2)'), 'Che Bakery'));
	});

});

describe('Registration', () => {

	const frontPage = new FrontPage();

	it('Registration: get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
	});

	it('Registration: registration', async () => {
		if (browser.userIsLogged) {
			await frontPage.logout();
		}
		$('language-selector + button ').click();
		await frontPage.registration();
		await browser.wait(EC.presenceOf($('user-dropdown-menu')));
		expect(await $('user-dropdown-menu .dropdown button').getText()).toMatch('Autotest');
	});

	it('Registration: errors in registration', async () => {
		await frontPage.logout();
		$('language-selector + button ').click();
		await frontPage.registration('autotest.devhouse@gmail.com');
		expect(EC.presenceOf($('.modal form ui-message')));
	});
});