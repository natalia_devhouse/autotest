const FrontPage = require('../page-objects/front-page');
const ProfilePage = require('../page-objects/profile-page');
const OverviewPage = require('../page-objects/overview-page');
const DeliveryPage = require('../page-objects/delivery-page');
const CakeBuilder = require('../page-objects/cake-builder');

const EC = protractor.ExpectedConditions,
	platform = browser.isMobile ? ' mobile' : ' desktop',
	waitTimeout = 7 * 1000;

describe('Account pages: Invoice info' + platform, function () {

	const frontPage = new FrontPage();
	const profilePage = new ProfilePage();

	it('Get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
	});

	it('Login/go to profile', async () => {
		if (!browser.userIsLogged) {
			$('language-selector + button + button').click();
			await frontPage.login();
			expect(await frontPage.loggedMenuBtn.getText()).toMatch('Autotest');
		} else {
			await frontPage.goToProfile();
		}
	});

	it('Invoice info', async () => {
		await profilePage.goToInvoiceInfo();
		expect(browser.getTitle()).toMatch('Payment Preferences | Cake it easy');

		await profilePage.changeInvoiceInfo();
		expect(await profilePage.companyName.getAttribute("value")).toMatch('MERCK AB');
		expect(await profilePage.contactPerson.getAttribute("value")).toMatch('Autotest Devhouse');

		await profilePage.removeInvoiceInfo();
		expect(await profilePage.companyName.getAttribute("value")).toMatch('Autotest Company');
		expect(await profilePage.userEmail.getAttribute("value")).toMatch('autotest.devhouse');
	});
});

describe('Account pages: Reminders', () => {

	const frontPage = new FrontPage();
	const profilePage = new ProfilePage();

	it('Get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
	});

	it('Login/go to profile', async () => {
		if (!browser.userIsLogged) {
			$('language-selector + button + button').click();
			await frontPage.login();
			expect(await frontPage.loggedMenuBtn.getText()).toMatch('Autotest');
		} else {
			await frontPage.goToProfile();
		}
	});

	it('Add new calendar on reminders page', async () => {
		await profilePage.goToRemindersPage();
		await profilePage.addCalendar();
		expect(EC.presenceOf($('.reminder:not(.new-reminder) .action-col i')));
	});

	it('Remove person from calendar', async () => {
		await $$('.reminder:not(.new-reminder) .action-col i').first().click();
	});

	it('Upload reminders', async () => {
		await $('.panel-box .pb-header button').click();
		await browser.wait(EC.presenceOf($('.modal')), waitTimeout, 'Wait for upload reminders modal is shown');
		await profilePage.uploadReminder(false);
		await $('.modal button').click();
		await profilePage.uploadReminder(true);
		expect(EC.presenceOf($('reminder:not(.new-reminder)')));
	});

	it('Edit calendar name', async () => {
		await browser.wait(EC.stalenessOf($('default-busy')), waitTimeout, 'Wait for busy loader disappears');
		await profilePage.editCalendarName();
		await browser.wait(EC.textToBePresentInElement($$('.panel-box ui-select button span').first(), 'Work birthdays'),
			waitTimeout, 'Wait for calendar name is changed');
		expect(await $$('.panel-box ui-select button span').first().getText()).toMatch('Work birthdays');
	});

	it('Remove calendar', async () => {
		await profilePage.removeCalendar();
		expect(EC.stalenessOf($('.new-reminder'))).toBeTruthy();
	});
});

describe('Account pages: Delivery addresses', () => {
	const frontPage = new FrontPage();
	const profilePage = new ProfilePage();

	it('Get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
	});

	it('Login/go to profile', async () => {
		if (!browser.userIsLogged) {
			$('language-selector + button + button').click();
			await frontPage.login();
			expect(await frontPage.loggedMenuBtn.getText()).toMatch('Autotest');
		} else {
			await frontPage.goToProfile();
		}
	});

	it('Go to delivery address', async () => {
		await profilePage.goToDeliveryAddresses();
		await profilePage.clearDeliveryAddresses();
	});

	it('Add new delivery address', async () => {
		await profilePage.addDeliveryAddress();
		await browser.wait(EC.textToBePresentInElement($$('.pb-content .pb-table-lg tr:first-of-type td:first-child').first(), '3525'),
			waitTimeout, 'Wait for new delivery address is added');
		expect(await $$('.pb-content .pb-table-lg tr:first-of-type td:first-child').first().getText()).toMatch('3525');
	});

	it('Edit delivery address', async () => {
		await profilePage.editDeliveryAddress();
		await browser.wait(EC.textToBePresentInElement($$('.pb-content .pb-table-lg tr:first-of-type td:first-child').first(), '3524'),
			waitTimeout, 'Wait for delivey address is changed');
		expect(await $$('.pb-content .pb-table-lg tr:first-of-type td:first-child').first().getText()).toMatch('3524');
	});

});

describe('Account pages: Delivery address usage', () => {
	const frontPage = new FrontPage();
	const overviewPage = new OverviewPage();
	const deliveryPage = new DeliveryPage();
	const cakeBuilder = new CakeBuilder();

	it('Get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
		await frontPage.clearCart();
	});

	it('Find bakery', async () => {
		await frontPage.findBakery('cities-overview .search-location input');
		expect(await browser.getTitle()).toMatch('Order cake from Autotest bakery! | Cake it easy');
	});

	it('Open cake builder', async () => {
		if (browser.isMobile) {
			await overviewPage.openCakeBuilderMobile(1);
		} else {
			await overviewPage.openCakeBuilder(1);
		}
		expect($('.cake-builder-modal').isDisplayed()).toBeTruthy();
	});

	it('Customise cake', async () => {
		await cakeBuilder.chooseCake();
	});

	it('Go to delivery', async () => {
		await cakeBuilder.goToDelivery();
		expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
	});

	it('Home delivery', async () => {
		await deliveryPage.homeDeliveryWithSavedAddress();
		expect($('delivery-to-door [name=deliveryAddress]').getAttribute('value')).toMatch('Test street 26');
		expect($('delivery-to-door [name=userName]').getAttribute('value')).toMatch('Autotest');
		expect($('delivery-to-door [name=phone]').getAttribute('value')).toMatch('854-965-744');
	});
});

describe('Account pages: Default delivery address usage', () => {
	const frontPage = new FrontPage();
	const profilePage = new ProfilePage();
	const overviewPage = new OverviewPage();
	const deliveryPage = new DeliveryPage();
	const cakeBuilder = new CakeBuilder();

	it('Get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
	});

	it('Login/go to profile', async () => {
		if (!browser.userIsLogged) {
			$('language-selector + button + button').click();
			await frontPage.login();
			expect(await frontPage.loggedMenuBtn.getText()).toMatch('Autotest');
		} else {
			await frontPage.goToProfile();
		}
	});

	it('Add delivery address', async () => {
		await profilePage.goToDeliveryAddresses();
	});

	it('Make default delivery address', async () => {
		await profilePage.makeDefaultDeliveryAddress();
		expect($('.pb-content .pb-table-lg tr:first-child td:nth-child(4) span').getText()).toMatch('Default');
	});

	it('Go to store', async () => {
		await $('ui-back-button .back-button').click();
		await browser.wait(EC.titleContains('Order cakes online | Home delivery or pick-up | Cake it easy'),
			waitTimeout, 'Wait for redirect to front page and get correct title');
		await frontPage.clearCart();
	});

	it('Find bakery', async () => {
		await frontPage.findBakery('cities-overview .search-location input');
		expect(await browser.getTitle()).toMatch('Order cake from Autotest bakery! | Cake it easy');
	});

	it('Open cake builder', async () => {
		if (browser.isMobile) {
			await overviewPage.openCakeBuilderMobile(1);
		} else {
			await overviewPage.openCakeBuilder(1);
		}
		expect($('.cake-builder-modal').isDisplayed()).toBeTruthy();
	});

	it('Customise cake', async () => {
		await cakeBuilder.chooseCake();
	});

	it('Go to delivery', async () => {
		await cakeBuilder.goToDelivery();
		expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
	});

	it('Home delivery', async () => {
		await deliveryPage.homeDeliveryWithSavedAddress(true);
		expect($('delivery-to-door [name=deliveryAddress]').getAttribute('value')).toMatch('Test street 26');
		expect($('delivery-to-door [name=userName]').getAttribute('value')).toMatch('Autotest');
		expect($('delivery-to-door [name=phone]').getAttribute('value')).toMatch('854-965-744');
	});
});

describe('Account pages: Remove delivery address', () => {

	const frontPage = new FrontPage();
	const profilePage = new ProfilePage();

	it('Get main page', async () => {
		await frontPage.getPage();
		expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
	});

	it('Login/go to profile', async () => {
		if (!browser.userIsLogged) {
			$('language-selector + button + button').click();
			await frontPage.login();
			expect(await frontPage.loggedMenuBtn.getText()).toMatch('Autotest');
		} else {
			await frontPage.goToProfile();
		}
	});

	it('Add delivery address', async () => {
		await profilePage.goToDeliveryAddresses();
	});

	it('Remove delivery address', async () => {
		await profilePage.clearDeliveryAddresses();
		expect(EC.presenceOf($('.pb-content. text-center')));
	});

});