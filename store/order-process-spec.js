const FrontPage = require('../page-objects/front-page');
const OverviewPage = require('../page-objects/overview-page');
const DeliveryPage = require('../page-objects/delivery-page');
const CakeBuilder = require('../page-objects/cake-builder');
const PaymentPage = require('../page-objects/payment-page');
const CartActions = require('../page-objects/cart-actions');

const EC = protractor.ExpectedConditions,
    cakePrice = 300,
    withMotivePrice = cakePrice + 30,
    withTextPrice = withMotivePrice + 20,
    withExtraPrice = withTextPrice + 150,
    fullPrice = withExtraPrice + 160,
    platform = browser.isMobile ? ' mobile' : ' desktop',
    waitTimeout = 7 * 1000;

if (browser.browserName === "Safari") {
    browser.driver.manage().window().maximize();
}

describe('Order process: Order Pickup and Private Payment' + platform, () => {

    const frontPage = new FrontPage();
    const overviewPage = new OverviewPage();
    const deliveryPage = new DeliveryPage();
    const paymentPage = new PaymentPage();
    const cakeBuilder = new CakeBuilder();

    it('Get main page', async () => {
        await frontPage.getPage();
        expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
    });

    (browser.loginBeforeOrder && !browser.isMobile ? it: xit)('Login', async () => {
        $('language-selector + button + button').click();
        await frontPage.login();
        expect(await frontPage.loggedMenuBtn.getText()).toMatch('Autotest');
        await frontPage.getPage();
        await frontPage.clearCart();
    });

    it('Find bakery', async () => {
        await frontPage.findBakery();
        expect(await browser.getTitle()).toMatch('Order cake from Autotest bakery! | Cake it easy');
    });

    it('Add pieces cake with one variant to cart', async () => {
        if (browser.isMobile) {
            await overviewPage.openCakeBuilderMobile(0);
        } else {
            await overviewPage.addPiecesCake();
            expect($$('cart-btn > button').first().isDisplayed()).toBeTruthy();
        }
    });

    it('Go to delivery', async() => {
        if (browser.isMobile) {
            await cakeBuilder.goToDelivery();
        } else {
            await overviewPage.goToDeliveryFromCart();
        }
        expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
    });

    it('Pickup delivery', async () => {
        await deliveryPage.pickupDelivery();
        expect(await browser.getTitle()).toMatch('Choose payment | Cake it easy');
    });

    it('Payment', async () => {
        await paymentPage.choosePaymentPersonType(true);
    });

    // temporary replaced with bambora
    xit('Klarna Payment', async () => {
        await paymentPage.klarnaPayment();
        expect(await browser.getTitle()).toMatch('Receipt | Cake it easy');
    });
});

describe('Order process: Order Home Delivery and Business Card Payment' + platform, () => {

    const frontPage = new FrontPage();
    const overviewPage = new OverviewPage();
    const deliveryPage = new DeliveryPage();
    const cakeBuilder = new CakeBuilder();
    const paymentPage = new PaymentPage();

    it('Get main page', async () => {
        await frontPage.getPage();
        expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
        await frontPage.clearCart();
    });

    it('Find bakery', async () => {
        await frontPage.findBakery('cities-overview .search-location input');
        expect(await browser.getTitle()).toMatch('Order cake from Autotest bakery! | Cake it easy');
    });

    it('Open cake builder', async () => {
        if (browser.isMobile) {
            await overviewPage.openCakeBuilderMobile(1);
        } else {
            await overviewPage.openCakeBuilder(1);
        }
        expect($('.cake-builder-modal').isDisplayed()).toBeTruthy();
    });

    it('Customise cake', async () => {
        await cakeBuilder.chooseCake();
        await cakeBuilder.chooseImage();
        await cakeBuilder.addText();
        expect(await  cakeBuilder.totalSum.getText()).toMatch(withTextPrice.toString());
        await cakeBuilder.chooseExtraWithManyVariants();
        expect(await  cakeBuilder.totalSum.getText()).toMatch(withExtraPrice.toString());
        await cakeBuilder.chooseExtraWithOneVariant();
        expect(await  cakeBuilder.totalSum.getText()).toMatch(fullPrice.toString());
    });

    it('Go to delivery', async () => {
        await cakeBuilder.goToDelivery();
        if (!browser.isMobile) {
            expect(await  $$('.side-cart-wrapper .cart-component-footer div:last-child').first().getText()).toMatch(fullPrice.toString());
        }
        expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
    });

    it('Home delivery', async () => {
        await deliveryPage.homeDelivery();
        expect(await browser.getTitle()).toMatch('Choose payment | Cake it easy');
    });

    it('Payment', async () => {
        await paymentPage.choosePaymentPersonType(false);
        await paymentPage.choosePaymentType(true);
        expect($('bambora-payment').isDisplayed()).toBeTruthy();
        await paymentPage.cardPayment();
    });

    it('Bambora Checkout', () => {
        paymentPage.bamboraCheckout();
        expect(browser.getTitle()).toMatch('Receipt | Cake it easy');
    });

});

describe('Order process: Order Pickup and Business Invoice Payment' + platform, () => {

    const frontPage = new FrontPage();
    const overviewPage = new OverviewPage();
    const deliveryPage = new DeliveryPage();
    const paymentPage = new PaymentPage();
    const cakeBuilder = new CakeBuilder();

    it('Get main page', async () => {
        await frontPage.getPage();
        expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
        await frontPage.clearCart();
    });

    it('Find bakery', async () => {
        await frontPage.findBakeryFromHelpModal();
        expect(await browser.getTitle()).toMatch('Order cake from Autotest bakery! | Cake it easy');
    });

    it('Add pieces cake with one variant to cart', async () => {
        if (browser.isMobile) {
            await overviewPage.openCakeBuilderMobile(0);
        } else {
            await overviewPage.addPiecesCake();
            expect($$('cart-btn > button').first().isDisplayed()).toBeTruthy();
        }
    });

    it('Go to delivery', async() => {
        if (browser.isMobile) {
            await cakeBuilder.goToDelivery();
        } else {
            await overviewPage.goToDeliveryFromCart();
        }
        expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
    });

    it('Pickup delivery', async () => {
        await deliveryPage.pickupDelivery();
        expect(await browser.getTitle()).toMatch('Choose payment | Cake it easy');
    });

    it('Payment', async () => {
        await paymentPage.choosePaymentPersonType(false);
        await paymentPage.choosePaymentType(false);
        if (!browser.userIsLogged) {
            expect($('.modal register-form').isDisplayed()).toBeTruthy();
            $('.have-account-btn').click();
            await frontPage.login();
            if (!browser.isMobile) {
                expect(await frontPage.loggedMenuBtn.getText()).toMatch('Autotest');
            }
        }
    });

    it('Invoice payment', async () => {
        await paymentPage.invoicePayment();

        expect(await browser.getTitle()).toMatch('Receipt | Cake it easy');
    });
});

(browser.isMobile ? xdescribe : describe)('Order process: Cart actions', () => {
    const frontPage = new FrontPage();
    const overviewPage = new OverviewPage();
    const deliveryPage = new DeliveryPage();
    const cakeBuilder = new CakeBuilder();
    const cartActions = new CartActions();

    it('Get main page', async () => {
        await frontPage.getPage();
        expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
    });

    it('Find bakery', async () => {
        await frontPage.findBakery();
        expect(await browser.getTitle()).toMatch('Order cake from Autotest bakery! | Cake it easy');
    });

    it('Add cakes to cart', async () =>  {
        await overviewPage.addPiecesCake();
        expect($$('cart-btn > button').first().isDisplayed()).toBeTruthy();
        await overviewPage.openCakeBuilder(1);
        expect($('.cake-builder-modal').isDisplayed()).toBeTruthy();
        await cakeBuilder.chooseCake();
        await cakeBuilder.addMoreCakes();
    });

    it('Edit cake from overview page', async () => {
        await cartActions.editCake();
        await cakeBuilder.chooseImage();
        await cakeBuilder.saveCake();
    });

    it('Change quantity of cakes', async () => {
        await cartActions.changeQuantityOfCake();
    });

    it('Go to delivery', async () => {
        await overviewPage.goToDeliveryFromCart();
        expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
    });

    it('Pickup delivery', async () => {
        await deliveryPage.pickupDelivery();
        expect(await browser.getTitle()).toMatch('Choose payment | Cake it easy');
    });

    it('Edit delivery', async () => {
        await cartActions.editDelivery();
        expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
    });

    it('Home delivery', async () => {
        await deliveryPage.homeDelivery();
        expect(await browser.getTitle()).toMatch('Choose payment | Cake it easy');
    });

    it('Delete cake from cart', async () => {
        await cartActions.deleteCake();
    });

    it('Clear cart', async () => {
        await cartActions.clearCart();
    });

    it('Try to add cake from other bakery', async () => {
        await overviewPage.addPiecesCake();
        await frontPage.findBakery('.search-location input', 'Che Bakery');
        await overviewPage.openCakeBuilder(0);
        await cakeBuilder.chooseCake();
        await $$('.build-section-footer .btn-footer').get(1).click();
        expect(EC.presenceOf($('.modal:not(.cake-builder-modal)')));
    });

    it('Work of modal for change cart to other bakery', async () => {
        await $('.modal:not(.cake-builder-modal) .modal-footer button:last-child').click();
        expect(EC.stalenessOf($('.modal:not(.cake-builder-modal)')));
        await browser.wait(EC.presenceOf($('default-busy')), waitTimeout, 'Wait for busy loader is shown');
        await browser.wait(EC.stalenessOf($('default-busy')), waitTimeout, 'Wait for busy loader disappears');
        await $$('.build-section-footer .btn-footer').get(1).click();
        expect(EC.presenceOf($('.modal:not(.cake-builder-modal)')));
        await browser.sleep(2000);
        await $$('.modal:not(.cake-builder-modal) .modal-footer button:first-child').first().click();
        await browser.wait(EC.stalenessOf($('.cake-builder-modal')), waitTimeout, 'Wait for cake builder is closed');
        await browser.wait(EC.titleContains('Choose delivery | Cake it easy'), waitTimeout, 'Wait for url changed to delivery page');
        expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
        expect(EC.textToBePresentInElement($('.cl-cake-info > div:nth-child(2)'), 'Che Bakery'));
    });

});

(browser.isMobile ? xdescribe : describe)('Order process: Quick registration', () => {

    const frontPage = new FrontPage();
    const overviewPage = new OverviewPage();
    const deliveryPage = new DeliveryPage();
    const paymentPage = new PaymentPage();
    const cakeBuilder = new CakeBuilder();

    it('Get main page', async () => {
        await frontPage.getPage();
        expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
        await frontPage.clearCart();
        if (browser.userIsLogged) {
            await frontPage.logout();
        }
    });

    it('Find bakery', async () => {
        await frontPage.findBakeryFromHelpModal();
        expect(await browser.getTitle()).toMatch('Order cake from Autotest bakery! | Cake it easy');
    });

    it('Add pieces cake with one variant to cart', async () => {
        if (browser.isMobile) {
            await overviewPage.openCakeBuilderMobile(0);
        } else {
            await overviewPage.addPiecesCake();
            expect($$('cart-btn > button').first().isDisplayed()).toBeTruthy();
        }
    });

    it('Go to delivery', async() => {
        if (browser.isMobile) {
            cakeBuilder.goToDelivery();
        } else {
            await overviewPage.goToDeliveryFromCart();
        }
        expect(await browser.getTitle()).toMatch('Choose delivery | Cake it easy');
    });

    it('Pickup delivery', async () => {
        await deliveryPage.pickupDelivery();
        expect(await browser.getTitle()).toMatch('Choose payment | Cake it easy');
    });

    it('Payment', async () => {
        await paymentPage.choosePaymentPersonType(false);
        await paymentPage.choosePaymentType(true);
        expect($('bambora-payment').isDisplayed()).toBeTruthy();
        await paymentPage.cardPayment(true);
    });

    it('Bambora Checkout', async () => {
        await paymentPage.bamboraCheckout();
        expect(await browser.getTitle()).toMatch('Receipt | Cake it easy');
        expect(EC.presenceOf($('quick-register')));
    });

    it('Quick registration on receipt page', async () => {
        await $$('quick-register input').first().sendKeys('ciedevhouse');
        await $('quick-register button').click();
        await browser.wait(EC.presenceOf($('ui-message .text-primary')), waitTimeout, 'Wait foe success message appears');
        expect(EC.presenceOf($('ui-message .text-primary'))).toBeTruthy();
    });
});