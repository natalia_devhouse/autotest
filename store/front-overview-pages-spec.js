const FrontPage = require('../page-objects/front-page');
const OverviewPage = require('../page-objects/overview-page');

const EC = protractor.ExpectedConditions,
    platform = browser.isMobile ? ' mobile' : ' desktop',
    waitTimeout = 7 * 1000;

(browser.isMobile ? xdescribe : describe)('Front pages: actions from front page' + platform, () => {

    const frontPage = new FrontPage();
    const overviewPage = new OverviewPage();

    it('Get main page', async () => {
        await frontPage.getPage();
        expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
    });

    xit('Open bakery from bakery section', async () => {
        await browser.wait(EC.presenceOf(frontPage.bakeryItem), waitTimeout, 'Wait for presence of bakery logo in slider');
        // await browser.wait(EC.elementToBeClickable(frontPage.bakeryItem), waitTimeout, 'Wait for bakery logo in slider is clickable');
        await browser.sleep(1500);
        frontPage.bakeryItem.click();
        await browser.wait(EC.urlContains('order-cake'), waitTimeout, 'Wait for change url to bakery overview');
        await browser.wait(EC.presenceOf(overviewPage.bakeryOverviewTitle), waitTimeout, 'Wait for presence of title on bakery overview page');
        await browser.wait(EC.textToBePresentInElement(overviewPage.bakeryOverviewTitle, overviewPage.bakeryOverviewTitleText),
            waitTimeout, 'Wait for presence of title on bakery overview page');
        expect(await overviewPage.bakeryOverviewTitle.getText()).toMatch(overviewPage.bakeryOverviewTitleText);
        $('header-layout .header__logo').click();
        await browser.wait(frontPage.waitUrlChange(), waitTimeout, 'Wait for change url to front page');
    });

    it('Open city from cities section', async () => {
        await browser.wait(EC.presenceOf(frontPage.cityItem), waitTimeout, 'Wait for presence of city in the list');
        await browser.wait(EC.elementToBeClickable(frontPage.cityItem), waitTimeout, 'Wait for city in the list is clickable');
        await browser.sleep(500);
        frontPage.cityItem.click();
        await browser.wait(EC.urlContains('order-cake/oslo'), waitTimeout, 'Wait for change url to city overview');
        await browser.wait(EC.presenceOf(overviewPage.cityOverviewTitle), waitTimeout, 'Wait for presence of title on city overview page');
        expect(await overviewPage.cityOverviewTitle.getText()).toMatch(overviewPage.cityOverviewTitleText);
        await browser.wait(EC.invisibilityOf($('.cakey-loader')), waitTimeout, 'Wait for invisibility of cakes loader');
        await browser.wait(EC.presenceOf(overviewPage.getCake(0).$('.cake-card-footer')), waitTimeout, 'Wait for presence of cake on overview');
        $('header-layout .header__logo').click();
        await browser.wait(frontPage.waitUrlChange(), waitTimeout, 'Wait for change url to front page');
    });

    it('Open how it works modal', async () => {
        await browser.wait(EC.presenceOf(frontPage.howItWorksLink), waitTimeout, 'Wait for presence "How it works link" in footer');
        await browser.sleep(500);
        frontPage.howItWorksLink.click();
        await browser.wait(EC.presenceOf($('.how-it-works-modal')), waitTimeout, 'Wait for presence of "How it works" modal');
        expect($('.how-it-works-modal').isDisplayed()).toBeTruthy();
        $('.modal .icon-close').click();
    });

    it('Open faq', async () => {
        await browser.sleep(500);
        frontPage.helpLink.click();
        await browser.wait(EC.presenceOf($('.modal')), waitTimeout, 'Wait for presence of "Help" modal');
        await $('.modal .modal-header').getText().then((result) => {
            expect(result.toLowerCase()).toMatch('contact us');
        });
        $('.modal .icon-close').click();
    });
});

describe('Front pages: City Overview' + platform, () => {

    const frontPage = new FrontPage();
    const overviewPage = new OverviewPage();

    it('Get main page', async () => {
        await frontPage.getPage();
        expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
    });

    it('Find city', async () => {
        await frontPage.findCity();
        expect(await browser.getTitle()).toMatch('Order cake online | Cakes in Jevnaker | Cake it easy');
    });

    it('Open pickup map', async () => {
        await browser.wait(EC.invisibilityOf($('.cakey-loader')), waitTimeout, 'Wait for invisibility of cakes loader');
        await browser.wait(EC.presenceOf(overviewPage.getCake(0)), waitTimeout, 'Wait for presence of cake on cakes overview');
        await browser.wait(EC.elementToBeClickable(overviewPage.pickupLink), waitTimeout, 'Wait for pickup link under the cake is clickable');
        overviewPage.pickupLink.click();
        await browser.wait(EC.presenceOf($('.modal.show')), waitTimeout, 'Wait for pickup map modal is shown');
        await browser.wait(EC.presenceOf($('.snazzy-title')), waitTimeout, 'Wait for presence of outlet pin on the map');
        await browser.wait(EC.textToBePresentInElement($('.snazzy-title'), 'Autotest bakery!'), waitTimeout, 'Wait for correct outlet name is shown on map');
        expect(await  $('.snazzy-title > div').getText()).toMatch('Autotest bakery!');
        $$('.modal-body .icon-close').first().click();
    });

    it('Open delivery tooltip', async () => {
        await overviewPage.deliveryLink.click();
        await browser.wait(EC.visibilityOf(overviewPage.deliveryModal), waitTimeout, 'Wait for delivery check modal is shown');
        expect(await  overviewPage.checkDeliveryCitiesList.getText())
            .toMatch('Gran, Hole, Jevnaker, Ringerike, Sør-Aurdal');
        $('.modal .icon-close').click();
    });

    it('Check filters panel working', async () => {
        const imageFilter = browser.isMobile ?
            $('.mobile-search-filters .filters > div:nth-child(2) ui-checkbox') : $('.search-cakes-panel ui-checkbox-btn button'),
            allergensFilter = browser.isMobile ?
                $('.mobile-search-filters .filters > div:nth-child(1) ui-checkbox') : $('.search-cakes-panel ui-multi-select'),
            filtersButton = $('.search-filter'),
            applyFilter = $('.mobile-search-filters .footer button');

        if (browser.isMobile) {
            await browser.wait(EC.elementToBeClickable(filtersButton), waitTimeout, 'Wait for filters button is clickable');
            filtersButton.click();
            await browser.wait(EC.presenceOf($('.mobile-search-filters')), waitTimeout, 'Wait for filters modal is shown');
        }

        await browser.wait(EC.elementToBeClickable(imageFilter), waitTimeout, 'Wait for filter "Add photo on cake" is clickable');
        imageFilter.click();

        if (browser.isMobile) {
            await applyFilter.click();
        }

        await browser.wait(EC.presenceOf($('.btn-filter-panel')), waitTimeout, 'Wait for panel with active filters is appeared');
        await browser.wait(EC.invisibilityOf($('.cakey-loader')), waitTimeout, 'Wait for invisibility of cakes loader');
        await browser.wait(EC.presenceOf(overviewPage.getCake(0)), waitTimeout, 'Wait for filtered cakes are shown');
        expect(await overviewPage.getCake(0).$('.cake-name').getText()).toMatch('Macaron Cake');

        if (browser.isMobile) {
            await filtersButton.click();
            await allergensFilter.click();
            await applyFilter.click();
        } else {
            allergensFilter.$('button').click();
            await browser.wait(EC.visibilityOf($('.search-cakes-panel ui-multi-select .dropdown-menu')),
                waitTimeout, 'Wait for allergens filter dropdown is shown');
            allergensFilter.$$('.dropdown-menu ui-checkbox').first().click();
            allergensFilter.$('.dropdown-menu > div > div').click();
        }

        await browser.wait(EC.visibilityOf($('.no-cakes-found-text')), waitTimeout, 'Wait for no cakes found with filters');
        expect($('.no-cakes-found-text').isDisplayed()).toBeTruthy();
        $('.filter-tags-container .btn-filter-panel.btn-pink').click();
    });

    it('Check delivery', async () => {
        await browser.waitForAngularEnabled(true);
        const postNumberInput = $('post-number-delivery-check input'),
            searchButton = browser.isMobile ? $('post-number-delivery-check button.btn-lg') : $('post-number-delivery-check button.btn-slim'),
            checkResult = $('.found-city');
        if (browser.isMobile) {
            await $('.mobile-search-filters button:not(.search-filter)').click();
        } else {
            await $('delivery-check-panel #checkDeliveryDropdown').click();
        }
        postNumberInput.sendKeys('3520');
        expect(EC.presenceOf(searchButton));
        await browser.wait(EC.presenceOf(checkResult), waitTimeout, 'Wait for city name is shown on delivery check tooltip');
        expect(await checkResult.getText()).toMatch('Jevnaker');
        postNumberInput.clear();
        postNumberInput.sendKeys('0001');
        expect(EC.presenceOf(searchButton));
        await browser.wait(EC.presenceOf(checkResult),  waitTimeout, 'Wait for city name is shown on delivery check tooltip');
        expect(await checkResult.getText()).toMatch('Oslo');
        searchButton.click();
        await browser.wait(EC.urlContains('oslo'), waitTimeout, 'Wait for url change on city overview');
        await browser.wait(EC.presenceOf($('.btn-filter-panel')), waitTimeout, 'Wait for panel with active filters is shown');
        expect(await  $('.filter-tags-container .btn-filter-panel.btn-gray').getText()).toMatch('Delivery to');
    });
});

describe('Front pages: Bakery Profile' + platform, () => {

    const frontPage = new FrontPage();

    it('Get main page', async () => {
        await frontPage.getPage();
        expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
    });

    it('Find bakery', async () => {
        await frontPage.findBakery();
        expect(await browser.getTitle()).toMatch('Order cake from Autotest bakery! | Cake it easy');
    });

    it('Check delivery availability', async () => {
        const postCodeInput = $$('bakery-delivery-check input').first();
        if (browser.isMobile) {
            $('bakery-profile-collapser button') .click();
        }
        await browser.wait(EC.elementToBeClickable($('bakery-profile .delivery-info-switcher button:last-child')),
            waitTimeout, 'Wait for button "Check delivery availability" is clickable');
        await $('bakery-profile .delivery-info-switcher button:last-child').click();
        postCodeInput.sendKeys('6515');
        await browser.wait(EC.presenceOf($('bakery-delivery-check > div:nth-of-type(3)')),
            waitTimeout, 'Wait for "No delivery" message is shown');
        expect(await $('bakery-delivery-check > div:last-child').getText()).toMatch('No delivery');
        postCodeInput.clear();
        postCodeInput.sendKeys('3525');
        expect(EC.presenceOf($('bakery-delivery-check .delivery-days'))).toBeTruthy();
    });
});

(browser.isMobile ? xdescribe : describe)('Front pages: Registration', () => {

    const frontPage = new FrontPage();

    it('Get main page', async () => {
        await frontPage.getPage();
        expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
    });

    it('Registration', async () => {
        if (browser.userIsLogged) {
            await frontPage.logout();
        }
        $('language-selector + button ').click();
        await frontPage.registration();
        await browser.wait(EC.presenceOf($('user-dropdown-menu')), waitTimeout, 'Wait for user dropdown menu is shown');
        await browser.wait(EC.textToBePresentInElement(frontPage.loggedMenuBtn, 'Autotest'));
        expect(await frontPage.loggedMenuBtn.getText()).toMatch('Autotest');
    });

    it('Errors in registration', async () => {
        await $('ui-back-button a').click();
        await frontPage.logout();
        $('language-selector + button ').click();
        await frontPage.registration(browser.baseEmail);
        expect(EC.presenceOf($('.modal form ui-message')));
    });
});