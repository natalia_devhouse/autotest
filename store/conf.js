const JasmineReporters = require('jasmine-reporters'),
	HtmlScreenshotReporter = require("protractor-jasmine2-screenshot-reporter");

exports.config = {
	seleniumAddress: 'http://localhost:4444/wd/hub',
	framework: 'jasmine',
	// maxSessions: 1,
	multiCapabilities: [
		{
			'browserName': 'chrome',
			chromeOptions: {
				args: ["--headless", "--disable-gpu", "--window-size=1500,1500"]
			},
			specs: ['front-overview-pages-spec.js', 'order-process-spec.js', 'account-pages-spec.js']
		},
		// {
		// 	'browserName': 'safari',
		// 	specs: ['front-overview-pages-spec.js', 'order-process-spec.js', 'account-pages-spec.js']
		// },
		// {
		// 	'browserName': 'chrome',
		// 	chromeOptions: {
		// 		args: ["--disable-gpu", "--window-size=1500,1500"]
		// 	},
		// 	specs: ['front-overview-pages-spec.js']
		// },
		// {
		// 	'browserName': 'chrome',
		// 	'logName': 'iPhone',
		// 	chromeOptions: {
		// 		// args: ["--headless", "--disable-gpu"],
		// 		mobileEmulation: { "deviceName": "iPhone 5/SE" }
		// 	},
		// 	specs: ['front-overview-pages-spec.js', 'order-process-spec.js']
		// }
	],
	jasmineNodeOpts: {defaultTimeoutInterval: 40000},
	onPrepare: function () {

		browser.loginBeforeOrder = true;

		browser.driver.getCapabilities().then((caps) => {
			browser.browserName = caps.get('browserName');
			browser.isMobile = caps.get('mobileEmulationEnabled');

			if (browser.browserName === "Safari") {
				browser.baseEmail = 'autotest.devhouse+safari@gmail.com';
			} else if (browser.isMobile) {
				browser.baseEmail = 'autotest.devhouse+mobile@gmail.com';
			} else {
				browser.baseEmail = 'autotest.devhouse@gmail.com';
			}

			if (browser.browserName === "Safari") {
				browser.driver.manage().window().maximize();
			}
		});

		jasmine.getEnv().addReporter(new HtmlScreenshotReporter({
			dest: "screenshots"
		}));

		return browser.getProcessedConfig().then(function(config) {
			const browserName = config.capabilities.browserName;

			const junitReporter = new JasmineReporters.JUnitXmlReporter({
				consolidateAll: true,
				savePath: 'reports',
				// this will produce distinct xml files for each capability
				filePrefix: browserName + '-xmloutput',
				modifySuiteName: function(generatedSuiteName, suite) {
					return browserName + '-' + generatedSuiteName;
				}
			});
			jasmine.getEnv().addReporter(junitReporter);
		});

    },
	beforeLaunch: function () {
		const fs = require('fs'),
			rimraf = require('rimraf'),
			dir_scr = 'screenshots',
			dir_rep = 'reports';
		const errorCallback = function (err) {
			if (err) {
				console.log(err);
			} else {
				console.log('Operation with fs finished successfully');
			}
		};

        if (fs.existsSync(dir_scr)) {
            rimraf(dir_scr + '/*', errorCallback);
        } else {
            fs.mkdirSync(dir_scr);
        }
        if (fs.existsSync(dir_rep)) {
            rimraf(dir_rep + '/*', errorCallback);
        } else {
            fs.mkdirSync(dir_rep);
        }
	}
};