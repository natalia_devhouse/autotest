const PaymentPage = function () {

	const EC = protractor.ExpectedConditions,
		waitTimeout = 7 * 1000,
		longTimeout = 10 * 1000;

	this.bamboraIframe = browser.widgetTesting ? $('.embed-container iframe'): $('.bc-overlay-container .bc-iframe-container iframe');

	this.choosePaymentPersonType = async function (individual = true) {
		await browser.sleep(2000);
		if (individual) {
			$('choose-payment-person-type button:first-child').click();
		} else {
			$('choose-payment-person-type button:last-child').click();
		}
	};

	this.choosePaymentType = async function (card = true) {
        await browser.sleep(2000);
		if (card) {
			$('choose-payment-type ui-btn-two-lines:first-child button').click();
			await browser.wait(EC.presenceOf($('bambora-payment')), waitTimeout, 'Wait for bambora form is shown');
		} else {
			$('choose-payment-type ui-btn-two-lines:last-child button').click();
			if (!browser.userIsLogged) {
				await browser.wait(EC.presenceOf($('.modal')), waitTimeout, 'Wait for auth modal is shown');
				await browser.wait(EC.presenceOf($('.have-account-btn')),
					waitTimeout, 'Wait for "Do you already have account?" button is shown');
			}
		}
	};

	this.cardPayment = async function (newUser = false, company = true) {
		await browser.wait(EC.presenceOf($('input[name=userName]')), waitTimeout, 'Wait for card payment form input is shown');
		await browser.sleep(1000);
		if (!browser.userIsLogged) {
			if (company) {
                $('input[name=userCompanyName]').sendKeys('My company');
			}
			$('input[name=userName]').sendKeys('Natalia Autotest');
			if (newUser) {
				$('input[name=userEmail]').sendKeys('autotest.devhouse+' + Math.floor(100000 + Math.random() * 999999) + '@gmail.com');
			} else {
				$('input[name=userEmail]').sendKeys(browser.baseEmail);
			}
			$('input[name=userPhone]').sendKeys('159-623');
		}
		await $('ui-terms-and-conditions ui-checkbox i').click();
		await browser.wait(EC.elementToBeClickable($('payment-contact-info + div button')),
			waitTimeout, 'Wait for "Confirm" button is clickable');
		await browser.sleep(1000);
		await $('payment-contact-info + div button').click();
	};

	this.bamboraCheckout = function () {
		browser.sleep(2000);
		browser.wait(EC.presenceOf(this.bamboraIframe), longTimeout,
			'Wait for bambora iframe is shown');
		browser.switchTo().frame(this.bamboraIframe.getWebElement()).then(async function () {
			await browser.wait(EC.presenceOf($('#panel_paymentcard > div > div > bec-payment-card-start > form > bec-credit-card-form > div > div:nth-child(1) > h2 > a')),
				waitTimeout, 'Wait for bambora test cards are shown');
			await browser.sleep(2000);
			await $('#panel_paymentcard > div > div > bec-payment-card-start > form > bec-credit-card-form > div > div:nth-child(1) > h2 > a').click();
			await browser.sleep(1000);
			await $('#democard-controller > bec-demo-cards > button:nth-child(1)').click();
			await browser.sleep(1000);
			await $('#payBtn > div:nth-child(1)').click();
            await browser.switchTo().defaultContent();
			if (browser.widgetTesting) {
                await browser.switchTo().frame($('#iframe-container-cie-widget-18202015 iframe').getWebElement());
                await browser.wait(EC.presenceOf($('customer-details-partial')), longTimeout, 'Wait for order details are shown on receipt page');
			} else {
                await browser.wait(EC.urlContains('receipt'), longTimeout, 'Wait for redirect to the receipt page');
                await browser.wait(EC.presenceOf($('receipt .order-details')), longTimeout, 'Wait for order details are shown on receipt page');
			}
		});
	};

	this.invoicePayment = async function () {
		await browser.wait(EC.presenceOf($('ui-terms-and-conditions ui-checkbox')), waitTimeout,
			'Wait for terms and conditions checkbox is shown');
		$('ui-terms-and-conditions ui-checkbox i').click();
		browser.wait(EC.elementToBeClickable($('ui-terms-and-conditions + div button')), waitTimeout,
			'Wait for "Confirm" button is clickable');
		$('ui-terms-and-conditions + div button').click();
		await browser.wait(EC.urlContains('receipt'), waitTimeout, 'Wait for redirect to the receipt page');
		browser.wait(EC.presenceOf($('receipt .order-details')), waitTimeout, 'Wait for order details are shown on receipt page');
	};

	this.klarnaPayment = async function () {
		await browser.waitForAngularEnabled(false);
		await browser.wait(EC.presenceOf($('#klarna-checkout-iframe')), waitTimeout, 'Wait for klarna iframe is shown');
		await browser.switchTo().frame(element(by.tagName('iframe')).getWebElement());
		await browser.wait(EC.presenceOf($('#email')), waitTimeout, 'Wait for email input is shown');
		await $('#email').click().sendKeys('youremail@email.com');
		await $('#postal_code').click().sendKeys('0563');
		await $('#national_identification_number').click();
		await browser.sleep(5000); // wait autocomplete of other fields

		$('#national_identification_number').getAttribute('value').then(async function (result) {
			if (!result) {
				await $('#national_identification_number').click().clear().sendKeys('01087000571');
			}
		});

		$('#given_name').getAttribute('value').then(async function (result) {
			if (!result) {
				await $('#given_name').click().clear().sendKeys('Testperson-no');
			}
		});

		$('#family_name').getAttribute('value').then(async function (result) {
			if (!result) {
				await $('#family_name').click().sendKeys('Approved');
				await $('#street_address').click().sendKeys('Sæffleberggate 56');
				await $('#city').click().sendKeys('Oslo');
				await $('#phone').click().sendKeys('40 123 456');
			}
		});

		$('#street_address').getAttribute('value').then(async function (result) {
			if (!result) {
				await $('#street_address').click().sendKeys('Sæffleberggate 56');
				await $('#city').click().sendKeys('Oslo');
				await $('#phone').click().sendKeys('40 123 456');
			}
		});

		$('#city').getAttribute('value').then(async function (result) {
			if (!result) {
				await $('#city').click().sendKeys('Oslo');
				await $('#phone').click().sendKeys('40 123 456');
			}
		});

		$('#phone').getAttribute('value').then(async function (result) {
			if (!result) {
				await $('#phone').click().sendKeys('40 123 456');
			}
		});

		await browser.executeScript('document.querySelector("#button-primary").click()');
		await browser.sleep(5000);
		if (EC.presenceOf($('#button-primary[data-cid="am.continue_button"]'))) {
			await browser.executeScript('document.querySelector("#button-primary").click()');
			await browser.sleep(3000);
		}
		await browser.executeScript('document.querySelector("#buy-button-next button").click()');
		await browser.sleep(2000);
		await browser.wait(EC.urlContains('receipt'), longTimeout, 'Wait for redirect to the receipt page');
		await browser.switchTo().defaultContent();
		await browser.wait(EC.presenceOf($('receipt .order-details')), longTimeout, 'Wait for order details are shown on receipt page');
	};
};

module.exports = PaymentPage;