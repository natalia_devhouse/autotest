const FrontPage = function () {

	const EC = protractor.ExpectedConditions,
		waitTimeout = 7 * 1000;

	this.bakeryItem = $$('bakeries-slider a').first();
	this.cityItem = $$('cities-overview .city-btn').first();
	this.howItWorksLink = $('.footer .links-col-first + div .footer-link:first-child a');
	this.helpLink = $('.footer .links-col-first .footer-link:last-child a');
	this.faqLink = $('.footer .links-col-first .footer-link:first-child a');
	this.loggedMenuBtn = $$('user-dropdown-menu .dropdown button').first();

	this.waitUrlChange = function () {
		return function () {
			return browser.getCurrentUrl().then(function(url) {
				return url;
			});
		}
	};

	this.getPage = async function (url = 'http://web-store-dev.azurewebsites.net/') {
		await browser.waitForAngularEnabled(false); // because of setInterval in bakeries slider
		await browser.get(url);
		// await browser.get('http://localhost:4200/');
		await browser.wait(EC.titleContains('Cake it easy'), waitTimeout, 'Wait for correct title on front page"');
		await browser.wait(EC.presenceOf($('language-selector')), waitTimeout, 'Wait for language selector is shown');
		await $$('language-selector > div .btn:last-child').first().getAttribute('class').then(async (result) => {
			if (result.indexOf('text-muted') !== -1) {
				await $$('language-selector .btn.text-muted').first().click();
				await browser.wait(this.waitUrlChange(), waitTimeout, 'Wait for language is changed and page reload');
				await browser.wait(EC.titleContains('Cake it easy'), waitTimeout, 'Wait for correct titlt on front page');
			}
		});
	};

	this.findBakery = async function (selector = '.search-location input', bakery = 'Autotest bakery!') {
		browser.waitForAngularEnabled(false);
		const searchBar = $$(selector).first();
		await browser.wait(EC.presenceOf(searchBar), waitTimeout, 'Wait for search bar is shown');
		await browser.wait(EC.elementToBeClickable(searchBar), waitTimeout, 'Wait for ability to type in search input');
		await searchBar.click().sendKeys(bakery);
		await browser.wait(EC.textToBePresentInElement($$('.search-location .dropdown-menu .dropdown-item').first(), bakery),
			waitTimeout, 'Wait for search location is find and shown in dropdown menu of search bar');
		await searchBar.sendKeys(protractor.Key.ARROW_DOWN).sendKeys(protractor.Key.ENTER);
		await browser.wait(EC.titleContains('Order cake from ' + bakery + ' | Cake it easy'), waitTimeout, 'Wait for correct title on bakery page');
	};

	this.findCity = async function () {
		const searchBar = $$('.search-location input').first();
		await browser.wait(EC.presenceOf(searchBar), waitTimeout, 'Wait for search bar is shown');
		await searchBar.click().sendKeys('Jevnak');
		await browser.wait(EC.textToBePresentInElement($$('.search-location .dropdown-menu .dropdown-item').first(), 'Jevnaker'),
			waitTimeout, 'Wait for search location is find and shown in dropdown menu of search bar');
		await searchBar.sendKeys(protractor.Key.ARROW_DOWN).sendKeys(protractor.Key.ENTER);
		await browser.wait(EC.urlContains('jevnaker'), waitTimeout, 'Wait foe url is changed to founded city');
		await browser.wait(EC.titleContains('Order cake online | Cakes in Jevnaker | Cake it easy'),
			waitTimeout, 'Wait for correct title on city overview');
	};

	this.findBakeryFromHelpModal = async function () {
		await browser.wait(EC.presenceOf($('.footer')));
		$('.footer .links-col-first .footer-link:last-child a').click();
		await browser.wait(EC.presenceOf($('.modal')), waitTimeout, 'Wait for "Help" modal is shown');
		$('.modal .form-back-btn-chevron').click();
		await browser.wait(EC.visibilityOf($('.modal .search-location input')), waitTimeout, 'Wait for search input is shown on FAQ modal');
		await this.findBakery('.modal .search-location input');
	};

	this.goToProfile = async function () {
		await browser.wait(EC.presenceOf($('language-selector + user-dropdown-menu .dropdown > button')),
			waitTimeout, 'Wait for "Go to profile" button is shown in user dropdown menu');
		await $('language-selector + user-dropdown-menu .dropdown > button').click();
		await $('language-selector + user-dropdown-menu .dropdown-menu a').click();
	};

	this.login = async function () {
		await $('input[formcontrolname=email]').sendKeys(browser.baseEmail);
		await $('input[formcontrolname=password]').sendKeys('ciedevhouse');
		await browser.wait(EC.elementToBeClickable($('.modal-auth-controls .btn-pink')),
			waitTimeout, 'Wait for login button is clickable');
		await $('.modal-auth-controls .btn-pink').click();
		await browser.wait(EC.stalenessOf($('.modal-auth')), waitTimeout, 'Wait for auth modal is disappeared');
		await browser.wait(EC.presenceOf($('user-dropdown-menu')), waitTimeout, 'Wait for user info is appeared in header');
		await browser.wait(EC.textToBePresentInElement($('header-user-section user-dropdown-menu .dropdown > button'), 'Autotest'),
			waitTimeout, 'Wait for user name is shown in header user section');
		await browser.sleep(1000);
		browser.userIsLogged = true;
	};

	this.registration = async function (email) {
		if (!email) {
			email = 'autotest.devhouse+' + Math.floor(100000 + Math.random() * 999999) + '@gmail.com';
		}
		await $$('.modal form .form-group input').get(0).sendKeys('Autotest');
		await $$('.modal form .form-group input').get(1).sendKeys('Devhouse');
		await $$('.modal form .form-group input').get(2).sendKeys(email);
		await $$('.modal form .form-group input').get(3).sendKeys('ciedevhouse');
		await $$('.modal form .form-group input').get(4).sendKeys('ciedevhouse');
		await $$('.modal-auth-controls div button').get(1).click();
	};

	this.logout = async function () {
		await $('language-selector + user-dropdown-menu .dropdown > button').click();
		await $('language-selector +user-dropdown-menu .dropdown-menu button').click();
		await browser.wait(EC.presenceOf($('language-selector + button')), waitTimeout, 'Wait for login button is shown in header');
		browser.userIsLogged = false;
	};

	this.clearCart = async function () {
		await browser.sleep(1000);
		await $('cart-btn').isPresent().then(async (result) => {
			if (result) {
				$$('cart-btn > button').first().click();
				await browser.wait(EC.presenceOf($('.cart-wrapper')), waitTimeout, 'Wait for cart is shown');
				await browser.wait(EC.elementToBeClickable($('.cart-component-footer > div:last-child button')),
					waitTimeout, 'Wait for "Clear cart" button is clickable in cart');
				await browser.sleep(500);
				await $('.cart-component-footer > div:last-child button').click();
				await browser.wait(EC.stalenessOf($('cart-btn')), 10 * 1000, 'Wait for cart is disappeared');
			}
		});
	};
};

module.exports = FrontPage;