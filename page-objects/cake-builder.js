const CakeBuilder = function () {

    const EC = protractor.ExpectedConditions,
        waitTimeout  = 7 * 1000,
        longTimeout = 10 * 1000;

    this.totalSum = $('.build-section-footer .cake-total-sum > div');

    this.chooseCake = async function () {
        await $$('cake-sizes-section .size-shape').first().click();
        await browser.sleep(1000);
        await $$('cake-variants-section .radio-item').first().click();
        await browser.sleep(1000);
    };

    this.chooseImage = async function () {
        await $('cake-motive-section .build-section__content div:nth-child(2) .radio-item').click();
        await browser.wait(EC.presenceOf($('.modal:not(.cake-builder-modal)')), waitTimeout, 'Wait for image cropper modal is shown');
        await browser.executeScript('document.querySelector(".modal:not(.cake-builder-modal) input[type=file]").classList.remove("d-none")');
        await browser.sleep(1000);
        try {
            await $('.modal:not(.cake-builder-modal) input[type=file]').sendKeys('/Users/natalia/Downloads/images/autotest.jpg');
        }
        catch (err) {
            console.log(err);
        }
        await browser.wait(EC.presenceOf($('.modal:not(.cake-builder-modal) .modal-footer button:last-child')),
            waitTimeout, 'Wait for confirm button is shown on image cropper');
        await browser.sleep(1000);
        await $('.modal:not(.cake-builder-modal) .modal-footer button:last-child').click();
        await browser.wait(EC.invisibilityOf($('cake-motive-section lib-ng-busy')),
            longTimeout, 'Wait for invisibility of busy loader on cake motive section');
        await browser.wait(EC.textToBePresentInElement(this.totalSum, '330'),
            waitTimeout, 'Wait for total sum is correct after adding cake motive');
        await browser.sleep(1000);
    };

    this.chooseImageOnWidget = async function () {
        await $('cake-motive-section .build-section__content div:nth-child(2) .radio-item').click();
        await browser.wait(EC.presenceOf($('choose-motive-section')), waitTimeout, 'Wait for image cropper modal is shown');
        await browser.executeScript('document.querySelector("choose-motive-section input[type=file]").classList.remove("d-none")');
        await browser.sleep(1000);
        try {
            await $('choose-motive-section input[type=file]').sendKeys('/Users/natalia/Downloads/images/autotest.jpg');
        }
        catch (err) {
            console.log(err);
        }
        await browser.wait(EC.presenceOf($('choose-motive-section .modal-footer button:last-child')),
            waitTimeout, 'Wait for confirm button is shown on image cropper');
        await browser.sleep(1000);
        await $('choose-motive-section .modal-footer button:last-child').click();
        await browser.wait(EC.invisibilityOf($('cake-motive-section lib-ng-busy')),
            waitTimeout, 'Wait for invisibility of busy loader on cake motive section');
        await browser.sleep(1000);
    };

    this.addText = async function () {
        await browser.wait(EC.stalenessOf($('cake-text-section .blocker-layer')), waitTimeout, 'Wait for block layer disappears from text section');
        await $('cake-text-section textarea').sendKeys('Congratilutions!');
        await browser.sleep(1000);
    };

    this.chooseExtraWithOneVariant = async function () {
        const radioExtra = $('cake-extra-products-section:last-of-type');
        await browser.wait(EC.elementToBeClickable(radioExtra.$('.ui-extra-products-collapser')),
            waitTimeout, 'Wait for extra products collapser is clickable');
        await radioExtra.$('.ui-extra-products-collapser').click();
        await browser.wait(EC.elementToBeClickable(radioExtra.$('.ui-extra-products-collapse-content > div > div:nth-child(3) label')),
            waitTimeout, 'Wait for radio button is clickable');
        await radioExtra.$('.ui-extra-products-collapse-content > div > div:nth-child(3) label').click();
    };

    this.chooseExtraWithManyVariants = async function () {
        const checkboxesExtra = $('cake-extra-products-section:first-of-type');
        await checkboxesExtra.$('.ui-extra-products-collapser').click();
        await browser.wait(EC.elementToBeClickable(checkboxesExtra.$('.ui-extra-products-collapse-content > div:nth-child(3) label')),
            waitTimeout, 'Wait for checkbox is clickable');
        await checkboxesExtra.$('.ui-extra-products-collapse-content > div:nth-child(3) label').click();
        await browser.wait(EC.elementToBeClickable(checkboxesExtra.$('.ui-extra-products-collapse-content > div:nth-child(5) label')),
            waitTimeout, 'Wait for checkbox is clickable');
        await checkboxesExtra.$('.ui-extra-products-collapse-content > div:nth-child(5) label').click();
        await checkboxesExtra.$('.collapse-apply').click();
        await browser.wait(EC.stalenessOf(checkboxesExtra.$('.ui-extra-products-collapser.expand-pink')),
            waitTimeout, 'Wait for select with checkboxes is closed');
    };

    this.goToDelivery = async function () {
        await $$('.build-section-footer .btn-footer').get(1).click();
        if (browser.widgetTesting) {
            await browser.wait(EC.presenceOf($('choose-delivery-type')), waitTimeout, 'Wait for redirect to delivery page');
        } else {
            await browser.wait(EC.urlContains('delivery'), waitTimeout, 'Wait for redirect to delivery page');
        }
    };

    this.addMoreCakes = async function () {
        await $$('.build-section-footer .btn-footer').get(0).click();
        await browser.wait(EC.presenceOf($('.cart-wrapper')), waitTimeout, 'Wait for cart is shown');
    };

    this.saveCake = async function () {
        await element(by.buttonText('Save')).click();
        await browser.wait(EC.presenceOf($('.cart-wrapper')), waitTimeout, 'Wait for cart is shown');
    };
};

module.exports = CakeBuilder;