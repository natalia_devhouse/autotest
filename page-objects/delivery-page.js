const DeliveryPage = function () {

	const EC = protractor.ExpectedConditions,
		waitTimeout = 7 * 1000;

	this.postNumber = browser.widgetTesting ? '0001' : '3525';

	this.pickupDelivery = async function () {
		await browser.wait(EC.elementToBeClickable($('.choose-delivery-tabs .d-block:first-child button')),
			waitTimeout, 'Wait for "Pickup in store" button is clickable');
		if (browser.browserName === "Safari") {
			await browser.sleep(2000);
		}
		await $('.choose-delivery-tabs .d-block:first-child button').click();
		await browser.wait(EC.presenceOf($('choose-outlet .card button')), waitTimeout, 'Wait for outlets list is shown');
		await $('choose-outlet .card button').click();
		if (browser.isMobile) {
			await browser.wait(EC.elementToBeClickable($('.ui-datepicker')), waitTimeout, 'Wait for calendar input is clickable');
			await $('.ui-datepicker').click();
		}
		await browser.wait(EC.presenceOf($('choose-outlet ngb-datepicker')), waitTimeout, 'Wait for datepicker is shown');
		await $$('choose-outlet ngb-datepicker .ngb-dp-day:not(.disabled)').first().click();
		if (browser.isMobile) {
			await $('.outlet-form .dropdown-toggle').click();
		}
		await $('.outlet-form .dropdown-menu .dropdown-item:first-child').click();
		await browser.wait(EC.presenceOf($('side-cart-wrapper .delivery-cart-line')), waitTimeout, 'Wait for delivery info is shown in the cart');
		await $('.outlet-form > div > div:last-child .btn').click();
		if (browser.widgetTesting) {
			await browser.wait(EC.presenceOf($('choose-payment-person-type')), waitTimeout, 'Wait for redirect to payment page');
		} else {
			await browser.wait(EC.urlContains('payment'), waitTimeout, 'Wait for redirect to payment page');
		}
	};

	this.homeDelivery = async function () {
		const deliveryForm = $('delivery-to-door');
		await browser.wait(EC.elementToBeClickable($('.choose-delivery-tabs .d-block:last-child button')),
			waitTimeout, 'Wait for "Pickup in store" button is clickable');
		if (browser.browserName === "Safari") {
			await browser.sleep(2000);
		}
		await $('.choose-delivery-tabs .d-block:last-child button').click();
		await browser.wait(EC.presenceOf(deliveryForm), waitTimeout, 'Wait for home delivery form is shown');

		deliveryForm.$('[name=postNumber]').clear().sendKeys(this.postNumber);
		await browser.wait(EC.presenceOf(deliveryForm.$('ngb-datepicker .ngb-dp-day')),
			waitTimeout, 'Wait for datepicker is shown');
		await deliveryForm.$$('ngb-datepicker .ngb-dp-day:not(.disabled)').first().click();
		await browser.wait(EC.presenceOf(deliveryForm.$('.dropdown-menu .dropdown-item')),
			waitTimeout, 'Wait for time dropdown is shown');
		deliveryForm.$('.dropdown-menu .dropdown-item:first-child').click();
		deliveryForm.$('[name=deliveryAddress]').sendKeys('Autotest Street 52');
		deliveryForm.$('[name=userName]').sendKeys('Natalia');
		deliveryForm.$('[name=phone]').sendKeys('741741741');
		browser.wait(EC.presenceOf($('side-cart-wrapper .delivery-cart-line')),
			waitTimeout, 'Wait for delivery info is shown in the cart');
		deliveryForm.$('form > div:last-child > button').click();
        if (browser.widgetTesting) {
            await browser.wait(EC.presenceOf($('choose-payment-person-type')), waitTimeout, 'Wait for redirect to payment page');
        } else {
            await browser.wait(EC.urlContains('payment'), waitTimeout, 'Wait for redirect to payment page');
        }
	};

	this.homeDeliveryWithSavedAddress = async function(defaultAddress) {
		const deliveryForm = $('delivery-to-door');
		await browser.wait(EC.elementToBeClickable($('.choose-delivery-tabs .d-block:last-child button')),
			waitTimeout, 'Wait for "Pickup in store" button is clickable');
		await $('.choose-delivery-tabs .d-block:last-child button').click();
		await browser.wait(EC.presenceOf(deliveryForm), waitTimeout, 'Wait for home delivery form is shown');
		if (!defaultAddress) {
			deliveryForm.$('.text-pink span:last-child').click();
			await browser.wait(EC.presenceOf($('.modal')), waitTimeout, 'Wait for modal with saved delivery addresses is shown');
			await $('.modal .card button').click();
			await browser.wait(EC.elementToBeClickable($$('.modal .card .btn-pink').first()),
				waitTimeout, 'Wait for "Continue" button for saved delivery address is clickable');
			await browser.sleep(500);
			await $$('.modal .card .btn-pink').first().click();
		}
		await browser.wait(EC.presenceOf(deliveryForm.$('ngb-datepicker .ngb-dp-day')),
			waitTimeout, 'Wait for datepicker is shown');
		await deliveryForm.$$('ngb-datepicker .ngb-dp-day:not(.disabled)').first().click();
		await browser.wait(EC.presenceOf(deliveryForm.$('.dropdown-menu .dropdown-item')),
			waitTimeout, 'Wait for time dropdown is shown');
		deliveryForm.$('.dropdown-menu .dropdown-item:first-child').click();
		await browser.wait(EC.presenceOf(deliveryForm.$('[name=deliveryAddress]')),
			waitTimeout, 'Wait for saved delivery address is filled to delivery form');
	};
};

module.exports = DeliveryPage;