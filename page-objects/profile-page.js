const ProfilePage = function () {

	const countrySelector = $('invoice-company-searcher ui-select'),
		orgNumber = $('invoice-company-searcher input'),
		companyName = $('invoice-company-searcher > div > .form-group:nth-child(3) input'),
		confirmInvoiceButton = $('invoice-company-searcher .text-right > button:last-child'),
		EC = protractor.ExpectedConditions,
		waitTimeout = 7 * 1000;

	this.companyName = $('[name=orgName]');
	this.userEmail = $('user-invoice-info [name=email]');
	this.contactPerson = $('user-invoice-info [name=contactPersonName]');

	this.goToInvoiceInfo = async function () {
		await browser.waitForAngularEnabled(true);
		await $('.sidebar-menu a:nth-child(4)').click();
	};

	this.changeInvoiceInfo = async function () {
		await $('[name=orgName] + div button').click();
		await countrySelector.$('.dropdown > button').click();
		await countrySelector.$('.dropdown-menu').element(by.partialButtonText('Norway')).click();
		await orgNumber.click();
		await orgNumber.sendKeys('979959478');
		await browser.wait(EC.presenceOf(companyName), waitTimeout, 'Wait for company name input is shown');
		await browser.sleep(1500);
		await confirmInvoiceButton.click();
	};

	this.removeInvoiceInfo = async function () {
		await $$('user-invoice-info .text-right > button').first().click();
		await countrySelector.$('.dropdown > button').click();
		await countrySelector.$('.dropdown-menu').element(by.partialButtonText('Sweden')).click();
		await orgNumber.click();
		await orgNumber.sendKeys('9519519519');
		await browser.sleep(1500);
		await confirmInvoiceButton.click();
		await browser.wait(EC.presenceOf($('user-invoice-info ui-radio-group label')),
			waitTimeout, 'Wait for new invoice info is shown');
		await $('user-invoice-info ui-radio-group label:first-child').click();
	};

	this.goToRemindersPage = async function () {
		await $('.sidebar-menu a:nth-child(5)').click();
	};

	this.addCalendar = async function () {
		await browser.wait(EC.presenceOf($('default-busy')), waitTimeout, 'Wait for busy loader is shown');
		await browser.wait(EC.stalenessOf($('default-busy')), waitTimeout, 'Wait for busy loader disappears');
		await $('cake-reminders button').click();
		await $('upsert-calendar-modal input').sendKeys('Birthdays');
		await $('upsert-calendar-modal button').click();
		await browser.wait(EC.presenceOf($('cake-reminder input[name=firstName]')), waitTimeout, 'Wait for new reminder inputs are shown');
		await browser.wait(EC.stalenessOf($('default-busy')), waitTimeout, 'Wait for busy loader disappears');
		await browser.sleep(1000);
		await $('cake-reminder input[name=firstName]').sendKeys('Anna');
		await $('#date').click();
		await browser.sleep(500);
		await browser.wait(EC.presenceOf($('ngb-datepicker .ngb-dp-day')), waitTimeout, 'Wait for datepicker is shown');
		await $$('ngb-datepicker .ngb-dp-day').first().click();
		await $('cake-reminder .action-col button').click();
	};

	this.uploadReminder = async function (success) {
		await browser.wait(EC.elementToBeClickable($('.modal input[type=file] + button')), waitTimeout, 'Wait for upload button is clickable');
		await browser.executeScript('document.querySelector(".modal input[type=file]").style = "display: block"');
		await browser.sleep(1000);
		if (success) {
			try {
				await $('.modal input[type=file]').sendKeys('/Users/natalia/Downloads/reminders_success.xlsx');
			}
			catch (err) {
				console.log(err);
			}
			await browser.wait(EC.presenceOf($('.modal ui-message')), waitTimeout, 'Wait for error message about upload');
			await browser.wait(EC.stalenessOf($('.modal')));
		} else {
			try {
				await $('.modal input[type=file]').sendKeys('/Users/natalia/Downloads/reminders_error.xlsx');
			}
			catch (err) {
				console.log(err);
			}
			await browser.wait(EC.presenceOf($('.modal ui-message')), waitTimeout, 'Wait for success message about upload');
		}
	};

	this.editCalendarName = async function () {
		await $('.panel-box .btn-text.text-primary').click();
		await browser.wait(EC.presenceOf($('.modal')), waitTimeout, 'Wait for reminder edit modal');
		await $('.modal input').clear();
		await $('.modal input').sendKeys('Work birthdays');
		await $('.modal button').click();
	};

	this.removeCalendar = async function () {
		await $('.panel-box .btn-text.text-danger').click();
	};

	this.goToDeliveryAddresses = async function () {
		await $('.sidebar-menu a:nth-child(3)').click();
	};

	this.addDeliveryAddress = async function () {
		await $('delivery-address .pb-header button').click();
		await browser.wait(EC.presenceOf($('.modal')), waitTimeout, 'Wait for add delivery address modal is shown');
		await $$('.modal ui-select button').first().click();
		await $('.dropdown-menu button:first-child').click();
		await $('.modal [name=recipientsName]').sendKeys('Autotest');
		await $('.modal [name=streetName]').sendKeys('Test street 26');
		await $('.modal [name=postalCode]').sendKeys('3525');
		await $('.modal [name=phone]').sendKeys('854-965-744');
		await browser.wait(EC.elementToBeClickable($('.modal .btn-pink')), waitTimeout, 'Wait for confirm button is clickable');
		await browser.sleep(1000);
		await $('.modal .btn-pink').click();
		await browser.wait(EC.presenceOf($('.pb-content .pb-table-lg tr td .icon-trash')), waitTimeout, 'Wait for address is shown in table');
	};

	this.editDeliveryAddress = async function () {
		await $$('.pb-content table tr .btn-text-primary').first().click();
		await browser.wait(EC.presenceOf($('.modal')), waitTimeout, 'Wait for edit delivery address modal is shown');
		await $('.modal [name=postalCode]').clear();
		await $('.modal [name=postalCode]').sendKeys('3524');
		await browser.wait(EC.elementToBeClickable($('.modal .btn-pink')), waitTimeout, 'Wait for confirm button is clickable');
		await browser.sleep(1000);
		await $('.modal .btn-pink').click();
	};

	this.makeDefaultDeliveryAddress = async function () {
		// await browser.wait(EC.presenceOf($('.ng-busy-default-wrapper')), waitTimeout, 'Wait for busy loader is shown');
		// await browser.wait(EC.stalenessOf($('.ng-busy-default-wrapper')), waitTimeout, 'Wait for busy loader disappears');
		browser.sleep(2000);
		$('.pb-content .pb-table-lg tr:first-child td:nth-child(4) .btn-text-default').click();
		await browser.wait(EC.presenceOf($('.pb-content .pb-table-lg tr:first-child td:nth-child(4) span')),
			waitTimeout, 'Wait for address marked as default');
	};

	this.clearDeliveryAddresses = async function () {
		await browser.sleep(1500);
		$('.pb-content .pb-table-lg tr:first-child td .icon-trash').isPresent().then(async (result) => {
			if (result) {
				await $('.pb-content .pb-table-lg tr:first-child td .icon-trash').click();
				await browser.wait(EC.stalenessOf($('.pb-content .pb-table-lg tr')), waitTimeout, 'Wait for delivery table is empty');
			}
		});
	}
};

module.exports = ProfilePage;