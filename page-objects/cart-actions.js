const CartActions = function () {

	const EC = protractor.ExpectedConditions,
		waitTimeout = 7 * 1000;

	this.openCakeInfo = async function (count) {
		await browser.getCurrentUrl().then(async (result) => {
			if (result.includes('delivery') || result.includes('payment')) {
				await $$('.side-cart-wrapper .card').get(count).click();
			} else {
				await $$('.cart-wrapper .card').get(count).click();
			}
		});
	};

	this.editCake = async function () {
		await this.openCakeInfo(1);
		await browser.sleep(500);
		await element(by.partialButtonText('Edit choices')).click();
		await browser.wait(EC.presenceOf($('cake-motive-section')), waitTimeout, 'Wait for cake builder is opened');
	};

	this.deleteCake = async function () {
		await this.openCakeInfo(1);
		await $('.order-line-total + div div').click();
	};

	this.editDelivery = async function () {
		await $('.delivery-cart-line button').click();
		await browser.wait(EC.urlContains('delivery'), waitTimeout, 'Wait for redirect to delivery page');
	};

	this.clearCart = async function () {
		await $('.extra-clear-cart button').click();
		await browser.wait(EC.urlContains('autotest-bakery'), waitTimeout, 'Wait for redirect to bakery page');
	};

	this.changeQuantityOfCake = async function () {
		await this.openCakeInfo(0);
		await $$('.card .btn-count-control').get(1).click();
		await browser.actions().mouseMove($('.footer-logo')).perform();
		await browser.wait(EC.stalenessOf($('.cart-wrapper')), waitTimeout, 'Wait for cart hide after mouseout and timeout');
	};

};

module.exports = CartActions;