const OverviewPage = function () {
	const EC = protractor.ExpectedConditions,
		waitTimeout = 7 * 1000;
	this.bakeryOverviewTitle = $('.cakes-overview-wrap h2');
	this.bakeryOverviewTitleText = 'Available cakes from bakery';
	this.cityOverviewTitle = $('.cakes-overview-wrap h1');
	this.cityOverviewTitleText = 'Order cake in Oslo';
	this.checkDeliveryCitiesList = $('bakery-delivery-check > div:last-child');
	this.deliveryModal = $('.modal-delivery-check');

	this.getCake = function (count) {
		return $$('cakes-list cake-item').get(count);
	};

	this.addPiecesCake = async function (cakePosition = 0) {
		await browser.wait(EC.invisibilityOf($('.cakey-loader')), waitTimeout, 'Wait for invisibility of cakes loader');
		await browser.wait(EC.presenceOf(this.getCake(0)), waitTimeout, 'Wait for cakes are shown on overview page');
		await  browser.sleep(2000);
		await browser.actions().mouseMove(this.getCake(cakePosition).$('.cake-description')).perform();
		await this.getCake(cakePosition).$('.cake-description').click();
		await browser.wait(EC.elementToBeClickable(this.getCake(cakePosition).$('.pieces-overlay .text-center button:last-child')),
			waitTimeout, 'Wait for "Add pieces cake" button is clickable from overlay');
		await this.getCake(cakePosition).$('.pieces-overlay .text-center button:last-child').click();
		await browser.wait(EC.presenceOf($$('cart-btn > button').first()), waitTimeout, 'Wait for cart is shown');
	};

	this.openCakeBuilder = async function (count) {
		await browser.wait(EC.invisibilityOf($('.cakey-loader')), waitTimeout, 'Wait for invisibility of cakes loader');
		await browser.wait(EC.presenceOf(this.getCake(count)), waitTimeout, 'Wait for cakes are shown on overview page');
		await browser.actions().mouseMove(this.getCake(count).$('.cake-description')).perform();
		await this.getCake(count).$('.cake-description').click();
		await browser.wait(EC.presenceOf($('cake-sizes-section')), 10*1000, 'Wait for cake builder is shown');
	};

	this.openCakeBuilderMobile = async function (count) {
		await browser.wait(EC.invisibilityOf($('.cakey-loader')), waitTimeout, 'Wait for invisibility of cakes loader');
		await browser.wait(EC.presenceOf(this.getCake(count)), waitTimeout, 'Wait for cakes are shown on overview page');
		await this.getCake(count).click();
		await browser.wait(EC.presenceOf($('cake-builder')), waitTimeout, 'Wait for cake builder is shown');
	};

	this.goToDeliveryFromCart = async function () {
		await browser.waitForAngularEnabled(false);
		await browser.wait(EC.elementToBeClickable($$('cart-btn > button').first()), waitTimeout, 'Wait for cart button is clickable');
		await $$('cart-btn > button').first().click();
		await browser.wait(EC.presenceOf($('.cart-wrapper')), waitTimeout, 'Wait for cart is shown');
		await browser.wait(EC.elementToBeClickable($('.cart-component-footer > button')),
			waitTimeout, 'Wait for "Proceed to checkout" button is clickable in the cart');
		await browser.sleep(500);
		await $('.cart-component-footer > button').click();
        if (browser.widgetTesting) {
            await browser.wait(EC.presenceOf($('choose-delivery-type')), waitTimeout, 'Wait for redirect to delivery page');
        } else {
            await browser.wait(EC.urlContains('order-cake/delivery'), waitTimeout, 'Wait for redirect to delivery page');
        }
	};

	this.pickupLink = this.getCake(0).$('.cake-card-footer .cake-bakery-name + div > span:first-child span');
	this.deliveryLink = this.getCake(0).$('.cake-card-footer .cake-bakery-name + div > span:last-child');
};

module.exports = OverviewPage;