const FrontPage = require('../page-objects/front-page');
const OverviewPage = require('../page-objects/overview-page');
const DeliveryPage = require('../page-objects/delivery-page');
const CakeBuilder = require('../page-objects/cake-builder');
const PaymentPage = require('../page-objects/payment-page');

const EC = protractor.ExpectedConditions,
    waitTimeout = 7 * 1000,
    longTimeout = 10 * 1000,
    deliveryCartLine = $('.delivery-cart-line > div:nth-child(2) .mb-2'),
    dateInput = $('ui-delivery-datepicker-popup input'),
    bakeriesSlider = $('bakeries-slider');

describe('Front page screenshots', () => {

    const frontPage = new FrontPage();

    it('Front page screen', async () => {
        await frontPage.getPage();
        await browser.wait(EC.presenceOf(bakeriesSlider), waitTimeout, 'Wait for bakeries slider is shown');

        const bakerySliderClientRect = await browser.executeScript('return arguments[0].getBoundingClientRect()', bakeriesSlider);

        await browser.protractorImageComparison.checkFullPageScreen('frontPage', {
            blockOut: [
                {
                    x: bakerySliderClientRect.left,
                    y: bakerySliderClientRect.top,
                    width: bakerySliderClientRect.width,
                    height: bakerySliderClientRect.height
                },
            ],
        }).then((result) => {
            expect(result).toEqual(0);
        });
    });

    it('How it works modal screen', async () => {
        await browser.wait(EC.presenceOf(frontPage.howItWorksLink), waitTimeout, 'Wait for presence "How it works link" in footer');
        await browser.sleep(500);
        frontPage.howItWorksLink.click();
        await browser.wait(EC.presenceOf($('.how-it-works-modal')), waitTimeout, 'Wait for presence of "How it works" modal');
        await browser.protractorImageComparison.checkElement($('.how-it-works-modal .modal-content'), 'HowItWorksModal').then((result) => {
            expect(result).toEqual(0);
        });
        $('.modal .icon-close').click();
    });

    it('FAQ modal screen', async () => {
        await browser.sleep(500);
        frontPage.faqLink.click();
        await browser.wait(EC.presenceOf($('.modal')), waitTimeout, 'Wait for presence of "FAQ" modal');
        await browser.protractorImageComparison.checkElement($('.modal .modal-content'), 'FaqModal').then((result) => {
            expect(result).toEqual(0);
        });
        $('.modal .icon-close').click();
    });

    it('Help modal screen', async () => {
        await browser.sleep(500);
        frontPage.helpLink.click();
        await browser.wait(EC.presenceOf($('.modal')), waitTimeout, 'Wait for presence of "Help" modal');
        await browser.protractorImageComparison.checkElement($('.modal .modal-content'), 'HelpModal').then((result) => {
            expect(result).toEqual(0);
        });
        $('.modal .icon-close').click();
    });

    it('Auth modal screen', async () => {
        await $('language-selector + button').click();
        await browser.protractorImageComparison.checkElement($('.modal .modal-content'), 'SignUpModal').then((result) => {
            expect(result).toEqual(0);
        });
        await $('.have-account-btn').click();
        await browser.protractorImageComparison.checkElement($('.modal .modal-content'), 'LoginModal').then((result) => {
            expect(result).toEqual(0);
        });
        await $('.modal-auth-controls .btn-text-pink').click();
        await browser.protractorImageComparison.checkElement($('.modal .modal-content'), 'ForgotPasswordModal').then((result) => {
            expect(result).toEqual(0);
        });
        $('.modal .icon-close').click();
    });

});

describe('Bakery profile screenshots', () => {

    const frontPage = new FrontPage();

    it('Bakery page screen', async () => {
        await frontPage.getPage();
        await frontPage.findBakery();
        await browser.protractorImageComparison.checkFullPageScreen('bakeryPage').then((result) => {
            expect(result).toEqual(0);
        });
    });

    it('Check delivery screen', async () => {
        const postCodeInput = $$('bakery-delivery-check input').first();
        await browser.wait(EC.elementToBeClickable($('bakery-profile .delivery-info-switcher button:last-child')),
            waitTimeout, 'Wait for button "Check delivery availability" is clickable');
        await $('bakery-profile .delivery-info-switcher button:last-child').click();
        postCodeInput.sendKeys('6515');
        await browser.wait(EC.presenceOf($('bakery-delivery-check > div:nth-of-type(3)')),
            waitTimeout, 'Wait for "No delivery" message is shown');

        await browser.protractorImageComparison.checkElement($('.check-delivery-view'), 'checkDeliveryEmptyBakery').then((result) => {
            expect(result).toEqual(0);
        });

        postCodeInput.clear();
        postCodeInput.sendKeys('3525');
        await browser.wait(EC.presenceOf($('bakery-delivery-check .delivery-days')));

        await browser.protractorImageComparison.checkElement($('.check-delivery-view'), 'checkDeliveryBakery').then((result) => {
            expect(result).toEqual(0);
        });
    });

});

describe('Order process screenshots', () => {

    const frontPage = new FrontPage();
    const overviewPage = new OverviewPage();
    const deliveryPage = new DeliveryPage();
    const paymentPage = new PaymentPage();
    const cakeBuilder = new CakeBuilder();

    it('Get main page', async () => {
        await frontPage.getPage();
    });

    it('Find bakery', async () => {
        await frontPage.findBakery('cities-overview .search-location input');
    });

    it('Cake builder screens', async () => {
        await overviewPage.openCakeBuilder(1);
        await browser.protractorImageComparison.checkScreen('cakeBuilder').then((result) => {
            expect(result).toEqual(0);
        });
        await cakeBuilder.chooseCake();
        await browser.sleep(500);
        await browser.protractorImageComparison.checkScreen('cakeBuilderChosenSize').then((result) => {
            expect(result).toEqual(0);
        });
        await cakeBuilder.chooseImage();
        await browser.sleep(500);
        await browser.protractorImageComparison.checkScreen('cakeBuilderChosenImage').then((result) => {
            expect(result).toEqual(0);
        });
        await cakeBuilder.addText();
        await browser.sleep(500);
        await browser.protractorImageComparison.checkScreen('cakeBuilderAddedText').then((result) => {
            expect(result).toEqual(0);
        });
        await cakeBuilder.chooseExtraWithManyVariants();
        await browser.sleep(500);
        await browser.protractorImageComparison.checkScreen('cakeBuilderChosenExtraCheckbox').then((result) => {
            expect(result).toEqual(0);
        });
        await cakeBuilder.chooseExtraWithOneVariant();
        await browser.sleep(500);
        await browser.protractorImageComparison.checkScreen('cakeBuilderChosenExtraRadio').then((result) => {
            expect(result).toEqual(0);
        });
        await cakeBuilder.goToDelivery();
    });

    it('Delivery screenshots', async () => {
        await browser.protractorImageComparison.checkFullPageScreen('deliveryPageStart').then((result) => {
            expect(result).toEqual(0);
        });
        await deliveryPage.homeDelivery();
        await browser.wait(EC.elementToBeClickable($('.ui-back-button-content .back-button')));
        await $('.ui-back-button-content .back-button').click();

        await browser.wait(EC.presenceOf(dateInput));
        let inputClientRect = await browser.executeScript('return arguments[0].getBoundingClientRect()', dateInput);
        let cartDateClientRect = await browser.executeScript('return arguments[0].getBoundingClientRect()', deliveryCartLine);

        await browser.protractorImageComparison.checkFullPageScreen('deliveryPageHome', {
            blockOut: [
                { x: inputClientRect.left, y: inputClientRect.top, width: inputClientRect.width, height: inputClientRect.height },
                { x: cartDateClientRect.left, y: cartDateClientRect.top, width: cartDateClientRect.width, height: cartDateClientRect.height },
            ],
        }).then((result) => {
            expect(result).toEqual(0);
        });

        await deliveryPage.pickupDelivery();
        await browser.wait(EC.elementToBeClickable($('.ui-back-button-content .back-button')));
        await $('.ui-back-button-content .back-button').click();
        await browser.wait(EC.presenceOf(dateInput));

        inputClientRect = await browser.executeScript('return arguments[0].getBoundingClientRect()', dateInput);
        cartDateClientRect = await browser.executeScript('return arguments[0].getBoundingClientRect()', deliveryCartLine);

        await browser.protractorImageComparison.checkFullPageScreen('deliveryPagePickup', {
            blockOut: [
                { x: inputClientRect.left, y: inputClientRect.top, width: inputClientRect.width, height: inputClientRect.height },
                { x: cartDateClientRect.left, y: cartDateClientRect.top, width: cartDateClientRect.width, height: cartDateClientRect.height },
            ],
        }).then((result) => {
            expect(result).toEqual(0);
        });

        await $('.outlet-form > div > div:last-child .btn').click();
        await browser.wait(EC.urlContains('payment'), waitTimeout, 'Wait for redirect to payment page');
    });

    it('Payment screenshots', async () => {
        await browser.sleep(1500);
        let cartDateClientRect = await browser.executeScript('return arguments[0].getBoundingClientRect()', deliveryCartLine);
        await browser.protractorImageComparison.checkFullPageScreen('paymentPageStart', {
            blockOut: [
                { x: cartDateClientRect.left, y: cartDateClientRect.top, width: cartDateClientRect.width, height: cartDateClientRect.height },
            ],
        }).then((result) => {
            expect(result).toEqual(0);
        });

        // Temporary disable, now bambora is used for individual payment
        // await paymentPage.choosePaymentPersonType(true);
        // await browser.sleep(1000);
        // await browser.waitForAngularEnabled(false);
        // await browser.wait(EC.presenceOf($('#klarna-checkout-iframe')), waitTimeout, 'Wait for klarna iframe is shown');
        //
        // let cartDateClientRect = await browser.executeScript('return arguments[0].getBoundingClientRect()', deliveryCartLine);
        // await browser.protractorImageComparison.checkFullPageScreen('paymentPageIndividual', {
        //     blockOut: [
        //         { x: cartDateClientRect.left, y: cartDateClientRect.top, width: cartDateClientRect.width, height: cartDateClientRect.height },
        //     ],
        // }).then((result) => {
        //         	expect(result).toEqual(0);
        //        });

        await paymentPage.choosePaymentPersonType(false);
        await paymentPage.choosePaymentType(true);

        let scrollY = await browser.executeScript('return window.scrollY');
        cartDateClientRect = await browser.executeScript('return arguments[0].getBoundingClientRect()', deliveryCartLine);
        await browser.protractorImageComparison.checkFullPageScreen('paymentPageBusinessBambora', {
            blockOut: [
                { x: cartDateClientRect.left, y: cartDateClientRect.top + scrollY, width: cartDateClientRect.width, height: cartDateClientRect.height },
            ],
        }).then((result) => {
            expect(result).toEqual(0);
        });

        await paymentPage.choosePaymentPersonType(false);
        await paymentPage.choosePaymentType(false);
        if (!browser.userIsLogged) {
            expect($('.modal register-form').isDisplayed()).toBeTruthy();
            $('.have-account-btn').click();
            await frontPage.login();
            await browser.wait(EC.textToBePresentInElement(frontPage.loggedMenuBtn, 'Autotest'), longTimeout, 'Wait for user is logged in');
        }

        scrollY = await browser.executeScript('return window.scrollY');
        cartDateClientRect = await browser.executeScript('return arguments[0].getBoundingClientRect()',deliveryCartLine);
        await browser.protractorImageComparison.checkFullPageScreen('paymentPageBusinessInvoice', {
            blockOut: [
                { x: cartDateClientRect.left, y: cartDateClientRect.top + scrollY, width: cartDateClientRect.width, height: cartDateClientRect.height },
            ],
        }).then((result) => {
            expect(result).toEqual(0);
        });
    });

    it('Receipt screenshots', async () => {
        await paymentPage.invoicePayment();
        await browser.wait(EC.presenceOf($('.order-details')), waitTimeout, 'Wait for order details are shown on receipt page');
        const orderNumberClientRect = await browser.executeScript('return arguments[0].getBoundingClientRect()', $$('.order-details .mt-1').first());
        const orderTimeClientRect = await browser.executeScript('return arguments[0].getBoundingClientRect()', $$('.order-details .mt-1').get(2));
        const deliveryInfoClientRect = await browser.executeScript('return arguments[0].getBoundingClientRect()', $('.receipt-delivery'));

        await browser.protractorImageComparison.checkFullPageScreen('receiptPage', {
            blockOut: [
                { x: orderNumberClientRect.left, y: orderNumberClientRect.top, width: orderNumberClientRect.width, height: orderNumberClientRect.height },
                { x: orderTimeClientRect.left, y: orderTimeClientRect.top, width: orderTimeClientRect.width, height: orderTimeClientRect.height },
                { x: deliveryInfoClientRect.left, y: deliveryInfoClientRect.top, width: deliveryInfoClientRect.width, height: deliveryInfoClientRect.height },
            ],
        }).then((result) => {
            expect(result).toEqual(0);
        });
    });
});