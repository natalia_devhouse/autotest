const JasmineReporters = require('jasmine-reporters'),
    ProtractorImageComparison = require('protractor-image-comparison'),
    HtmlScreenshotReporter = require("protractor-jasmine2-screenshot-reporter");

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    framework: 'jasmine',
    multiCapabilities: [
        {
            'browserName': 'chrome',
            chromeOptions: {
                args: ["--headless", "--window-size=1500,1000"]
            },
            specs: ['visual-store-spec.js']
        }
    ],
    jasmineNodeOpts: {defaultTimeoutInterval: 40000},
    onPrepare: function () {

        browser.loginBeforeOrder = true;

        browser.driver.getCapabilities().then((caps) => {
            browser.browserName = caps.get('browserName');

            browser.browserName = caps.get('browserName');

            if (browser.browserName === "Safari") {
                browser.baseEmail = 'autotest.devhouse+safari@gmail.com';
            } else {
                browser.baseEmail = 'autotest.devhouse@gmail.com';
            }

            if (browser.browserName === "Safari") {
                browser.driver.manage().window().maximize();
            }
        });

        browser.protractorImageComparison = new ProtractorImageComparison(
            {
                baselineFolder: 'screen-comparison/baseline',
                screenshotPath: 'screen-comparison/',
                autoSaveBaseline: true
            }
        );

        jasmine.getEnv().addReporter(new HtmlScreenshotReporter({
            dest: "screenshots"
        }));

        return browser.getProcessedConfig().then(function(config) {
            const browserName = config.capabilities.browserName;

            const junitReporter = new JasmineReporters.JUnitXmlReporter({
                consolidateAll: true,
                savePath: 'reports',
                // this will produce distinct xml files for each capability
                filePrefix: browserName + '-xmloutput',
                modifySuiteName: function(generatedSuiteName, suite) {
                    return browserName + '-' + generatedSuiteName;
                }
            });
            jasmine.getEnv().addReporter(junitReporter);
        });

    },
    beforeLaunch: function () {
        const fs = require('fs'),
            rimraf = require('rimraf'),
            dir_scr = 'screenshots',
            dir_rep = 'reports';
        const errorCallback = function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log('Operation with fs finished successfully');
            }
        };

        if (fs.existsSync(dir_scr)) {
            rimraf(dir_scr + '/*', errorCallback);
        } else {
            fs.mkdirSync(dir_scr);
        }
        if (fs.existsSync(dir_rep)) {
            rimraf(dir_rep + '/*', errorCallback);
        } else {
            fs.mkdirSync(dir_rep);
        }
    }
};