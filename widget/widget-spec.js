const FrontPage = require('../page-objects/front-page');
const OverviewPage = require('../page-objects/overview-page');
const DeliveryPage = require('../page-objects/delivery-page');
const CakeBuilder = require('../page-objects/cake-builder');
const PaymentPage = require('../page-objects/payment-page');

browser.widgetTesting = true;

const EC = protractor.ExpectedConditions,
    cakePrice = 315,
    withMotivePrice = cakePrice + 0,
    withTextPrice = withMotivePrice + 0,
    fullPrice = withTextPrice + 150,
    waitTimeout = 7 * 1000,
	longTimeout = 10 * 1000;

describe('Filtering', () => {

    const frontPage = new FrontPage();
    const overviewPage = new OverviewPage();

    it('Get main page', async () => {
        await frontPage.getPage();
        expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
    });

    it('Open widget', async () => {
        await browser.wait(EC.presenceOf($('.call-modal-btn')), longTimeout, 'Wait for widget button is displayed');
        await $('.call-modal-btn').click();
        await browser.wait(EC.presenceOf($('#iframe-container-cie-widget-18202015')), longTimeout, 'Wait for widget is shown');
        await browser.switchTo().frame($('#iframe-container-cie-widget-18202015 iframe').getWebElement());
    });

    it('Filter panel working', async () => {

        const imageFilter = $('.search-cakes-panel ui-checkbox-btn button'),
            allergensFilter = $('.search-cakes-panel ui-multi-select');

        await browser.wait(EC.invisibilityOf($('.cakey-loader')), waitTimeout, 'Wait for invisibility of cakes loader');
        await browser.wait(EC.presenceOf(overviewPage.getCake(0)), waitTimeout, 'Wait for cakes are shown on overview page');
        await browser.wait(EC.elementToBeClickable(imageFilter), waitTimeout, 'Wait for filter "Add photo on cake" is clickable');
        imageFilter.click();
        await browser.wait(EC.presenceOf($('.btn-filter-panel')), waitTimeout, 'Wait for panel with active filters is appeared');
        await browser.wait(EC.invisibilityOf($('.cakey-loader')), waitTimeout, 'Wait for invisibility of cakes loader');
        await browser.wait(EC.presenceOf(overviewPage.getCake(0)), waitTimeout, 'Wait for filtered cakes are shown');
        expect(await overviewPage.getCake(1).$('.cake-name').getText()).toMatch('Luganokake');

        allergensFilter.$('button').click();
        await browser.wait(EC.visibilityOf($('.search-cakes-panel ui-multi-select .dropdown-menu')),
            waitTimeout, 'Wait for allergens filter dropdown is shown');
        allergensFilter.$$('.dropdown-menu ui-checkbox').get(1).click();
        allergensFilter.$('.dropdown-menu > div > div').click();
        await browser.wait(EC.visibilityOf($('.no-cakes-found-text')), waitTimeout, 'Wait for no cakes found with filters');
        expect($('.no-cakes-found-text').isDisplayed()).toBeTruthy();

        $('.filter-tags-container .btn-filter-panel.btn-pink').click();
        await browser.wait(EC.presenceOf(overviewPage.getCake(0)), waitTimeout, 'Wait for filter are resetted');
        expect(EC.presenceOf(overviewPage.getCake(0)));
    });

});

describe('Pickup and Individual payment', () => {

    const frontPage = new FrontPage();
	const overviewPage = new OverviewPage();
	const deliveryPage = new DeliveryPage();
	const paymentPage = new PaymentPage();
	const cakeBuilder = new CakeBuilder();

    it('Get main page', async () => {
        await frontPage.getPage();
        expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
    });

    it('Open widget', async () => {
        await browser.wait(EC.presenceOf($('.call-modal-btn')), longTimeout, 'Wait for widget button is displayed');
        await browser.sleep(1500);
        await $('.call-modal-btn').click();
        await browser.wait(EC.presenceOf($('#iframe-container-cie-widget-18202015')), longTimeout, 'Wait for widget is shown');
        await browser.switchTo().frame($('#iframe-container-cie-widget-18202015 iframe').getWebElement());
    });

	it('Go to cake builder', async () => {
		await overviewPage.openCakeBuilder(0);
		expect(EC.presenceOf($('cake-builder'))).toBeTruthy();
	});

	it('Customize cake', async () => {
		await cakeBuilder.chooseCake();
        expect(await cakeBuilder.totalSum.getText()).toMatch(cakePrice.toString());
		await cakeBuilder.chooseImageOnWidget();
        expect(await  cakeBuilder.totalSum.getText()).toMatch(withMotivePrice.toString());
		await cakeBuilder.addText();
        expect(await  cakeBuilder.totalSum.getText()).toMatch(withTextPrice.toString());
		await cakeBuilder.chooseExtraWithOneVariant();
        expect(await  cakeBuilder.totalSum.getText()).toMatch(fullPrice.toString());
	});

	it('Go to delivery', async () => {
		await cakeBuilder.goToDelivery();
		expect(EC.presenceOf($('choose-delivery-type'))).toBeTruthy();
	});

	it('Pickup delivery', async () => {
		await deliveryPage.pickupDelivery();
		expect(EC.presenceOf($('choose-payment-person-type'))).toBeTruthy();
	});

	it('Individual payment', async () => {
		await paymentPage.choosePaymentPersonType(true);
		await paymentPage.cardPayment(false, false);
		expect(EC.presenceOf(paymentPage.bamboraIframe)).toBeTruthy();
	});

	it('Bambora Payment', async () => {
		await paymentPage.bamboraCheckout();
		expect(EC.presenceOf($('customer-details-partial'))).toBeTruthy();
	});
});

describe('Home delivery and Company payment', () => {

    const frontPage = new FrontPage();
	const overviewPage = new OverviewPage();
	const deliveryPage = new DeliveryPage();
	const paymentPage = new PaymentPage();

    it('Get main page', async () => {
        await frontPage.getPage();
        expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
    });

    it('Open widget', async () => {
        await browser.wait(EC.presenceOf($('.call-modal-btn')), longTimeout, 'Wait for widget button is displayed');
        await $('.call-modal-btn').click();
        await browser.wait(EC.presenceOf($('#iframe-container-cie-widget-18202015')), longTimeout, 'Wait for widget is shown');
        await browser.switchTo().frame($('#iframe-container-cie-widget-18202015 iframe').getWebElement());
    });

	it('Add pieces cake to cart', async () => {
		await overviewPage.addPiecesCake(7);
        expect($$('cart-btn > button').first().isDisplayed()).toBeTruthy();
	});

	it('Go to delivery', async () => {
        await overviewPage.goToDeliveryFromCart();
        expect(EC.presenceOf($('choose-delivery-type'))).toBeTruthy();
	});

	it('Home delivery', async () => {
		await deliveryPage.homeDelivery();
        expect(EC.presenceOf($('choose-payment-person-type'))).toBeTruthy();
	});

	it('Payment', async () => {
		await paymentPage.choosePaymentPersonType(false);
		await paymentPage.cardPayment();
        expect(EC.presenceOf(paymentPage.bamboraIframe)).toBeTruthy();
	});

	it('Bambora Checkout', async () => {
		await paymentPage.bamboraCheckout();
        expect(EC.presenceOf($('customer-details-partial'))).toBeTruthy();
	});
});