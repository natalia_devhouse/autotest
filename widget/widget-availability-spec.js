const FrontPage = require('../page-objects/front-page');
const OverviewPage = require('../page-objects/overview-page');

const EC = protractor.ExpectedConditions,
    waitTimeout = 7 * 1000,
    longTimeout = 10 * 1000,
    websites = ['http://web-store-dev.azurewebsites.net/', 'http://dev.cakeiteasy.no', 'http://dev.cakeiteasy.se'];

websites.forEach((url) => {
    describe('Widget: open widget from ' + url, () => {

        const frontPage = new FrontPage(),
            overviewPage = new OverviewPage();

        it('Get main page', async () => {
            await frontPage.getPage(url);
            expect(await browser.getTitle()).toMatch('Order cakes online | Home delivery or pick-up | Cake it easy');
        });

        it('Open widget', async () => {
            await browser.wait(EC.presenceOf($('.call-modal-btn')), waitTimeout, 'Wait for widget button is displayed');
            await $('.call-modal-btn').click();
            await browser.wait(EC.presenceOf($('#iframe-container-cie-widget-18202015')), longTimeout, 'Wait for widget is shown');
            browser.switchTo().frame($('#iframe-container-cie-widget-18202015 iframe').getWebElement()).then(async function () {
                await browser.wait(EC.presenceOf(overviewPage.getCake(0)), longTimeout, 'Wait for widget is shown');
            });
        });
    });
});